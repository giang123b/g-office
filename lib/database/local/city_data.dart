import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

import 'package:g_office/model/params/city_param.dart';

class CityDataLocal {
  CityDataLocal.cityDataLocal();

  static final CityDataLocal instance = CityDataLocal.cityDataLocal();
  Future<List> init() async {
    final data = await rootBundle.loadString('assets/city.json');
    final parseData = json.decode(data);
    return (parseData as List)
        .map((e) => CityParam.fromJson(e as Map<String, dynamic>))
        .toList();
  }
}
