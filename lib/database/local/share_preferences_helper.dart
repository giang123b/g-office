import 'package:g_office/utils/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  // static const _token = '_token';
  static const _introKey = '_introKey';

  static const _authKey = '_authKey';
  static const __idCompanyKey = '_idCompanyKey';
  //Get authKey
  static Future<String> getApiTokenKey() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString(_authKey) ?? "";
    } catch (e) {
      logger.e(e);
      return "";
    }
  }

  //Set authKey
  static Future<void> setApiTokenKey(String apiTokenKey) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(_authKey, apiTokenKey);
  }

  static Future<void> removeApiTokenKey() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(_authKey);
  }

  //Get intro
  static Future<bool> isSeenIntro() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getBool(_introKey) ?? false;
    } catch (e) {
      logger.e(e);
      return false;
    }
  }

  //Set intro
  static Future<void> setSeenIntro({bool isSeen = true}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_introKey, isSeen);
  }

  //Set key company
  static Future<void> setKeyCompany(String idCompany) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(__idCompanyKey, idCompany);
  }

  static Future<String?> getKeyCompany() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(__idCompanyKey);
  }
}
