
import 'package:json_annotation/json_annotation.dart';

part 'email_param.g.dart';

@JsonSerializable()
class EmailParam{

  @JsonKey()
  String? email;
  EmailParam({this.email});

  factory EmailParam.fromJson(Map<String, dynamic> json) =>
      _$EmailParamFromJson(json);

  Map<String,dynamic> toJson() => _$EmailParamToJson(this);
}
