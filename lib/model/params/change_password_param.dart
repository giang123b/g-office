import 'package:json_annotation/json_annotation.dart';

part 'change_password_param.g.dart';

@JsonSerializable()
class PasswordParam{
  @JsonKey()
  String? id;
  @JsonKey()
  String? newPassword;
  @JsonKey()
  String? confirmPassword;

  PasswordParam({this.id,this.confirmPassword,this.newPassword});

  factory PasswordParam.fromJson(Map<String, dynamic> json) =>
      _$PasswordParamFromJson(json);

  Map<String,dynamic> toJson() => _$PasswordParamToJson(this);
}
