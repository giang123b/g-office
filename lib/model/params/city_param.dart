class CityParam {
  final String? id;
  final String? name;

  CityParam({
    this.id,
    this.name,
  });

  factory CityParam.fromJson(Map<String, dynamic> json) {
    return CityParam(
      id: json['id'] as String,
      name: json['name'] as String,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
      };

  @override
  String toString() {
    return 'City(id:$id,name:$name)';
  }
}
