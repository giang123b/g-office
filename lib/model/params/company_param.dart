import 'package:json_annotation/json_annotation.dart';

part 'company_param.g.dart';

@JsonSerializable()
class CompanyParam {
  @JsonKey()
  final int? code;
  @JsonKey()
  final String? name;
  @JsonKey()
  final String? message;

  CompanyParam({
    this.message,
    this.name,
    this.code,
  });

  factory CompanyParam.fromJson(Map<String, dynamic> json) =>
      _$CompanyParamFromJson(json);

  Map<String, dynamic> toJson() => _$CompanyParamToJson(this);
}
