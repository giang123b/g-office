import 'package:json_annotation/json_annotation.dart';

part 'confirm_password_params.g.dart';

@JsonSerializable()
class ConfirmPasswordParam{
  @JsonKey()
  String? id;
  @JsonKey()
  String? password;

  ConfirmPasswordParam({this.id,this.password});

  factory ConfirmPasswordParam.fromJson(Map<String, dynamic> json) =>
      _$ConfirmPasswordParamFromJson(json);

  Map<String,dynamic> toJson() => _$ConfirmPasswordParamToJson(this);
}
