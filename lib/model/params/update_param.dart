import 'package:json_annotation/json_annotation.dart';
part 'update_param.g.dart';

@JsonSerializable()
class UpdateParam {
  @JsonKey()
  String? staffId;
  @JsonKey()
  String? name;
  @JsonKey()
  String? email;
  @JsonKey()
  String? gender;
  @JsonKey()
  String? birthDay;
  @JsonKey()
  String? address;
  @JsonKey()
  String? phoneNumber;
  @JsonKey()
  String? province;
  @JsonKey()
  String? residence;
  @JsonKey()
  String? idCard;
  @JsonKey()
  String? idDate;
  @JsonKey()
  String? idAddress;
  @JsonKey()
  String? taxCode;
  @JsonKey()
  String? maritalStatus;
  @JsonKey()
  String? nationality;
  @JsonKey()
  String? description;
  @JsonKey()
  String? vssId;
  @JsonKey()
  String? avatar;

  UpdateParam(
      {this.staffId,
      this.name,
      this.email,
      this.gender,
      this.birthDay,
      this.address,
      this.phoneNumber,
      this.province,
      this.residence,
      this.idCard,
      this.idDate,
      this.idAddress,
      this.taxCode,
      this.maritalStatus,
      this.nationality,
      this.description,
      this.vssId,
      this.avatar});
  factory UpdateParam.fromJson(Map<String, dynamic> json) =>
      _$UpdateParamFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateParamToJson(this);
}
