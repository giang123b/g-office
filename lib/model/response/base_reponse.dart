import 'package:json_annotation/json_annotation.dart';

part 'base_reponse.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class BaseResponseEntity<T> {
  @JsonKey()
  int? code;
  @JsonKey()
  String? message;
  @JsonKey()
  T? data;

  BaseResponseEntity({this.code, this.message, this.data});

  factory BaseResponseEntity.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$BaseResponseEntityFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$BaseResponseEntityToJson(this, toJsonT);
}
