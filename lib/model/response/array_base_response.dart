import 'package:json_annotation/json_annotation.dart';

part 'array_base_response.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ArrayBaseResponseEntity<T> {
  @JsonKey()
  int? code;
  @JsonKey()
  String? message;
  @JsonKey(defaultValue: [])
  List<T>? data;

  ArrayBaseResponseEntity({this.code, this.message, this.data});

  factory ArrayBaseResponseEntity.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$ArrayBaseResponseEntityFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$ArrayBaseResponseEntityToJson(this, toJsonT);
}
