import 'package:g_office/ui/pages/change_password/confirm_password/confirm_password_view.dart';
import 'package:g_office/ui/pages/change_password/change_password_success/change_password_success_view.dart';
import 'package:g_office/ui/pages/change_password/change_password_view.dart';
import 'package:g_office/ui/pages/forgot_password/forgot_password_view.dart';
import 'package:g_office/ui/pages/home/home_root/home_root_view.dart';
import 'package:g_office/ui/pages/home/tab_home/todo/todo_list_view.dart';
import 'package:g_office/ui/pages/home/tab_profile/contact_screen/contact_support_page.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/profile_view.dart';
import 'package:g_office/ui/pages/home/tab_profile/policy_screen/policy_screen.dart';
import 'package:g_office/ui/pages/login_user/input_company_page/input_company_screen.dart';
import 'package:g_office/ui/pages/login_user/login_page/login_view.dart';
import 'package:g_office/ui/pages/login_user/onboarding/on_boarding_view.dart';
import 'package:g_office/ui/pages/login_user/splash_page/splash_page.dart';
import 'package:get/get.dart';

class RouteConfig {
  ///main page
  static const String splash = "/splash";
  static const String login = "/login";
  static const String homeRoot = "/homeRoot";
  static const String onBoarding = "/onBoarding";
  static const String forgotPassword = "/forgotPassword";
  static const String changePassword = "/changePassword";
  static const String confirmChangePassword = '/confirmPW';
  static const String changePasswordSuccess = "/changePasswordSuccess";
  static const String inputCompany = "/input_company";
  static const String profile = '/profile';
  static const String policy = '/policy';
  static const String contact = '/contact';
  static const String todoList = '/todoList';

  ///Alias ​​mapping page
  static List<GetPage> getPages = [
    GetPage(name: splash, page: () => SplashScreen()),
    GetPage(name: inputCompany, page: () => InputCompanyScreen()),
    GetPage(name: login, page: () => LoginScreen()),
    GetPage(name: homeRoot, page: () => HomeRootPage()),
    GetPage(name: onBoarding, page: () => OnBoardingPage()),
    GetPage(name: forgotPassword, page: () => ForgetPasswordPage()),
    GetPage(name: changePassword, page: () => ChangePasswordPage()),
    GetPage(name: confirmChangePassword, page: () => ConfirmPasswordScreen()),
    GetPage(
        name: changePasswordSuccess, page: () => ChangePasswordPageSuccess()),
    GetPage(name: profile, page: () => ProfileScreen()),
    GetPage(name: policy, page: () => PolicyScreen()),
    GetPage(name: contact, page: () => ContactSupportScreen()),
    GetPage(name: todoList, page: () => TodoListPage()),
  ];
}
