import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppCustomTextStyle {
  static const TextStyle fontBigStyle = TextStyle(
    color: AppColors.textBlack,
    fontSize: 24,
    fontWeight: FontWeight.normal,
  );

  static const TextStyle fontTitle = TextStyle(
    color: AppColors.textBlack,
    fontSize: 17,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle fontHeadline1 = TextStyle(
    color: AppColors.silverSand,
    fontSize: 15,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle BODY_REGULAR = TextStyle(
    color: AppColors.text200,
    fontSize: 16,
    fontWeight: FontWeight.w400,
    height: 1.6,
    letterSpacing: 0.3,
  ); // regular

  static const TextStyle BODY_MEDIUM = TextStyle(
    color: AppColors.text300,
    fontSize: 16,
    fontWeight: FontWeight.w500,
    height: 26 / 16,
    letterSpacing: 0.003,
  );
  static const TextStyle fontHeadline2 = TextStyle(
    color: AppColors.textBlack,
    fontSize: 20,
    fontWeight: FontWeight.w400,
  );
}
