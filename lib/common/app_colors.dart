import 'dart:ui';

class AppColors {
  ///Common
  static const Color primary = Color(0xFF1a222d);
  static const Color main1 = Color(0xFFF58634);

  ///Background
  static const Color background = Color(0xFFFFFFFF);
  static const Color background2 = Color(0xffF5F6F7);
  static const Color backgroundLighter = Color(0xFF1f2837);
  static const Color backgroundDarker = Color(0xff111821);

  ///Shadow
  static const Color shadow = Color(0x25606060);

  ///Border
  static const Color border = Color(0xFF606060);

  ///Divider
  static const Color divider = Color(0xFF606060);

  ///Text
  static const Color textWhite = Color(0xFFFFFFFF);
  static const Color textBlack = Color(0xFF000000);
  static const Color textBlue = Color(0xFF0000FF);
  static const Color textDisable = Color(0xFF89a3b1);
  static const Color hintStyle = Color(0xFFA1A1A1);


  /// Tabs
  static const Color imageBG = Color(0xFF919191);

  ///BottomNavigationBar
  static const Color bottomNavigationBar = Color(0xFF919191);

  /// Main Color
  static const Color vividRed = Color(0xffFF1010);
  static const Color greenSuccess = Color(0xFF5AB163);
  static const Color frenchBlue = Color(0xff006FBD);

  static const Color silverSand = Color(0xffC4C4C4);
  static const text300 = Color(0xFF1B1D28);
  static const text200 = Color(0xFF8A8C9E);
}
