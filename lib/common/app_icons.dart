class AppIcons {
  static const String _baseLocalPath = 'assets/icons/';

  //icon show/hiden password
  static const String hidenPassword = '${_baseLocalPath}close_eye.svg';
  static const String showPassword = '${_baseLocalPath}open_eye.svg';
  static const String error = '${_baseLocalPath}error.svg';
  static const String camera = '${_baseLocalPath}camera.svg';
  static const String personal = '${_baseLocalPath}personal.svg';
  static const String icDefaultAvatar =
      '${_baseLocalPath}ic_default_avatar.svg';
  static const String success = '${_baseLocalPath}success.svg';
  static const String arrowLeft = '${_baseLocalPath}ic_arrow_left.svg';
  static const String icBiometric = '${_baseLocalPath}ic_biomereic.svg';
  static const String icChangePassword =
      '${_baseLocalPath}ic_change_password.svg';
  static const String icLogOut = '${_baseLocalPath}ic_log_out.svg';
  static const String icPolicy = '${_baseLocalPath}ic_policy.svg';
  static const String icUserInformation =
      '${_baseLocalPath}ic_user_information.svg';
  static const String icContact = '${_baseLocalPath}ic_contract.svg';

  //icon for tab-home-view
  static const String icHRM = '${_baseLocalPath}HRM.svg';
  static const String icACM = '${_baseLocalPath}ACM.svg';
  static const String icCRM = '${_baseLocalPath}CRM.svg';
  static const String icInsight = '${_baseLocalPath}Insight.svg';
  static const String icTicket = '${_baseLocalPath}Ticket.svg';
  static const String icTodo = '${_baseLocalPath}Todo.svg';

  //picturefor tabhomeview
  static const String picHrm = '${_baseLocalPath}pic_hrm.svg';
  static const String picAcm = '${_baseLocalPath}pic_acm.svg';
  static const String picTodo = '${_baseLocalPath}orange.svg';
  static const String picTicket = '${_baseLocalPath}pic_ticket.svg';
  static const String picCrm = '${_baseLocalPath}pic_crm.svg';
  static const String picInsight = '${_baseLocalPath}pic_insight.svg';

  //icon for home-root-view
  static const String icHome = '${_baseLocalPath}ic_home.svg';
  static const String icHomeSelected = '${_baseLocalPath}ic_home_selected.svg';
  static const String icNotification = '${_baseLocalPath}ic_notification.svg';
  static const String icNotificationSelected = '${_baseLocalPath}ic_notification_selected.svg';
  static const String icProfile = '${_baseLocalPath}ic_profile.svg';
  static const String icProfileSelected = '${_baseLocalPath}ic_profile_selected.svg';
  static const String icWorkTime = '${_baseLocalPath}ic_worktime.svg';
  static const String icWorkTimeSelected = '${_baseLocalPath}ic_worktime_selected.svg';

  //icon for todo-list-view
  static const String icPlus = '${_baseLocalPath}ic_plus.svg';

  static const String icRightArrow = '${_baseLocalPath}ic_right_arrow.svg';
  static const String icRemove = '${_baseLocalPath}ic_remove.svg';
  static const String icDropDown = '${_baseLocalPath}ic_drop_down.svg';
  static const String icCalendar = '${_baseLocalPath}ic_calendar.svg';
}
