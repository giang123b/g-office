import 'package:g_office/model/params/city_param.dart';
import 'package:intl/intl.dart';

extension StringExtension on String? {
  DateTime get parseStringToDateTimeYMD {
    return DateFormat("yyyy-MM-dd").parse(this ?? '0000-00-00');
  }

  DateTime? get parseStringToDateTimeDMY {
    return DateFormat("dd/MM/yyyy").parse(this ?? '00/00/0000');
  }

  String? getNameCity(List<CityParam> cityData) {
    for (final city in cityData) {
      if (city.id == this) {
        return city.name;
      }
    }
    return cityData[0].name;
  }
}

extension DateTimeExtension on DateTime? {
  String? get parseDateTimeToStringDMY {
    return DateFormat("dd/MM/yyyy").format(this ?? DateTime.now());
  }

  String get parseDateTimeToStringYMD {
    return DateFormat("yyyy-MM-dd").format(this ?? DateTime.now());
  }
}
