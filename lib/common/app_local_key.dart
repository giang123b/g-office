class LocalKeys {
  static const String keyCompany = 'key_company';
  static const String keySavedIdCompany = 'key_after_remove';
  static const String userInformation = 'userInformation';
  static const String isUseOnBoarding = 'isUseOnBoarding';
  static const String dateTimeCheckIn = 'isDateTimeCheckIn';
}
