class AppImages {
  static const String rootImage = 'assets/images/';

  /// Common
  static const bgImagePlaceholder = 'assets/images/bg_image_placeholder.png';
  static const icLogo = '${rootImage}ic_logo.png';
  static const icLogoTransparent = '${rootImage}ic_logo_transparent.png';
  static const icAvatar = '${rootImage}ic_avatar.png';
  static const icLogoGoffice = '${rootImage}ic_logo_goffice.png';
  static const imageBanner1 = '${rootImage}sample_image_1.jpeg';
  static const imageBanner2 = '${rootImage}sample_image_2.jpeg';
  static const imageBanner3 = '${rootImage}sample_image_3.jpeg';
  static const imageBanner4 = '${rootImage}sample_image_4.jpeg';
  static const imageBanner5 = '${rootImage}sample_image_5.jpeg';
  static const icSuccess = '${rootImage}ic_success.png';
  static const backGroundHome = '${rootImage}bg_home.png';
  static const icLogoApp = 'assets/icons/icon_app.png';
}
