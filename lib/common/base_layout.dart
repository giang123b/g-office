// Flutter imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BaseLayout extends StatelessWidget {
  final Widget body;
  final double padding;
  final Color color;
  final String tittle;
  final String extraActionContent;
  const BaseLayout({
    required this.body,
    this.padding = 20,
    this.color = const Color(0xFFFFFFFF),
    this.tittle = '',
    this.extraActionContent = '',
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: () {
        if (tittle != '') {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(56),
              child: Container(
                child: () {
                  if (tittle != '') {
                    return SizedBox(
                      width: double.infinity,
                      child: Container(
                        height: 56,
                        padding: const EdgeInsets.only(
                          left: 20,
                          right: 20,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                                onTap: () => Get.back(),
                                child: const SizedBox(
                                    height: 24,
                                    width: 24,
                                    child: Icon(Icons.arrow_back_ios))),
                            Text(
                              tittle,
                              style: const TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                            if (extraActionContent != '')
                              Text(
                                extraActionContent,
                                style: const TextStyle(fontSize: 17),
                              )
                            else
                              const SizedBox(
                                height: 24,
                                width: 24,
                              )
                          ],
                        ),
                      ),
                    );
                  } else {
                    Container();
                  }
                }(),
              ),
            ),
            body: Container(
              height: Get.height,
              color: color,
              padding: EdgeInsets.only(left: padding, right: padding),
              child: body,
            ),
          );
        } else {
          return Container(
            height: Get.height,
            color: color,
            padding: EdgeInsets.only(left: padding, right: padding),
            child: body,
          );
        }
      }(),
    );
  }
}
