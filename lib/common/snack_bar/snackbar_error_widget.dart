import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class SnackbarErrorWidget {
  static void callSnackBar(String? text) {
    Get.rawSnackbar(
        messageText: Text(
          text ?? '',
          style: AppCustomTextStyle.fontBigStyle
              .copyWith(fontSize: 15, color: AppColors.textWhite),
        ),
        snackPosition: SnackPosition.TOP,
        backgroundColor: AppColors.vividRed,
        margin: const EdgeInsets.symmetric(horizontal: 16),
        icon: SvgPicture.asset(AppIcons.error),
        borderRadius: 12);
  }
}
