import 'package:flutter/material.dart';

class AppDivider extends Divider {
  final double? paddingleft;
  final double? paddingRight;

  const AppDivider({this.paddingleft = 0, this.paddingRight = 0})
      : super(
          color: Colors.grey,
          height: 1,
          indent: paddingleft,
          endIndent: paddingRight,
        );
}
