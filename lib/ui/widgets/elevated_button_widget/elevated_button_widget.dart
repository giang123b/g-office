import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/ui/pages/login_user/login_page/logic/login_state.dart';

class ElevatedButtonWidget extends StatelessWidget {
  const ElevatedButtonWidget(
      {Key? key,
      this.width = double.infinity,
      this.height = 64,
      required this.informationButton,
      required this.onTap,
      this.color,
      required this.state})
      : super(key: key);
  final double width;
  final double height;
  final Function() onTap;
  final String informationButton;
  final Color? color;
  final ButtonState state;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          elevation: 0.0,
          splashFactory: NoSplash.splashFactory,
          minimumSize: Size(width, height),
          primary: color ??
              AppColors.frenchBlue.withOpacity(state.isActive ? 1 : 0.3),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          )),
      onPressed: state.isActive ? onTap : (){},
      child: Text(informationButton),
    );
  }
}
