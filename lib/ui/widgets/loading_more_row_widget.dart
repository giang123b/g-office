import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';

class LoadingMoreRowWidget extends StatelessWidget {
  final double height;
  final Color color;

  const LoadingMoreRowWidget(
      {this.height = 80, this.color = AppColors.main1});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: Align(
        child: SizedBox(
          width: 24,
          height: 24,
          child: CircularProgressIndicator(
            backgroundColor: color,
            valueColor:
                const AlwaysStoppedAnimation<Color>(AppColors.main1),
          ),
        ),
      ),
    );
  }
}
