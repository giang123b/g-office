import 'package:flutter/material.dart';
import 'package:g_office/common/app_images.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({Key? key, this.widthLogo, this.heightLogo})
      : super(key: key);
  final double? widthLogo;
  final double? heightLogo;
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      AppImages.icLogoGoffice,
      width: widthLogo ?? 150,
      height: heightLogo ?? 40,
    );
  }
}
