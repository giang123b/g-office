import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:flutter_svg/svg.dart';

class CircleAvatarWidget extends StatelessWidget {
  const CircleAvatarWidget({
    required this.linkIcon,
    Key? key,
    this.minRadius,
    this.colorBackground,
  }) : super(key: key);
  final String linkIcon;
  final double? minRadius;
  final Color? colorBackground;
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: colorBackground ?? AppColors.frenchBlue.withOpacity(0.7),
      minRadius: minRadius ?? 40,
      child: SvgPicture.asset(
        linkIcon,
      ),
    );
  }
}
