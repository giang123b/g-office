import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:get/get.dart';

class LoadingScreenApp {
  LoadingScreenApp._privateLoading();
  static final LoadingScreenApp instance = LoadingScreenApp._privateLoading();

  void showDialogLoading() {
    Get.dialog(WillPopScope(
      onWillPop: () async => false,
      child: const AlertDialog(
        elevation: 0,
        backgroundColor: Colors.transparent,
        content: Center(
          child: CircularProgressIndicator(
            color: AppColors.vividRed,
          ),
        ),
      ),
    ));
  }

  void hide() {
    if (Get.isDialogOpen ?? true) {
      Get.back();
    }
  }
}
