import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';

class AppCircularProgressIndicator extends StatelessWidget {
  final Color color;

  const AppCircularProgressIndicator({this.color = AppColors.main1});

  @override
  Widget build(BuildContext context) {
    return Align(
      child: SizedBox(
        width: 24,
        height: 24,
        child: CircularProgressIndicator(
          backgroundColor: color,
          valueColor: const AlwaysStoppedAnimation<Color>(AppColors.main1),
        ),
      ),
    );
  }
}
