import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingRowWidget extends StatelessWidget {
  final EdgeInsets? padding;
  final double height;

  const LoadingRowWidget({this.padding, this.height = 100});

  @override
  Widget build(BuildContext context) {
    final padding = this.padding ??
        const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 6,
        );
    return SizedBox(
      height: height,
      child: Padding(
        padding: padding,
        child: Shimmer.fromColors(
          baseColor: Colors.grey[350]!,
          highlightColor: Colors.grey[100]!,
          child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white,
              )),
        ),
      ),
    );
  }
}
