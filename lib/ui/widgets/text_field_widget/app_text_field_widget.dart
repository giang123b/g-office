import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:flutter_svg/svg.dart';

class AppTextFieldWidget extends StatefulWidget {
  const AppTextFieldWidget({
    Key? key,
    required this.onFieldSubmitted,
    this.validator,
    this.obsecureText,
    this.showIconSvg,
    this.keyboardType,
    this.nameSvg,
    this.onClickIcon,
    this.onChanged,
    this.lengthText,
    this.initValue,
    this.textInputAction,
    this.onTapField,
    this.enabled,
    this.readOnly = false,
    this.hintText,
    this.fillColor,
  }) : super(key: key);
  final Function(String) onFieldSubmitted;
  final bool? obsecureText;
  final bool? showIconSvg;
  final String? Function(String?)? validator;
  final TextInputType? keyboardType;
  final String? nameSvg;
  final Function()? onClickIcon;
  final Function()? onTapField;
  final Function(String)? onChanged;
  final int? lengthText;
  final String? initValue;
  final TextInputAction? textInputAction;
  final bool? enabled;
  final bool readOnly;
  final String? hintText;
  final Color? fillColor;
  @override
  _AppTextFieldWidgetState createState() => _AppTextFieldWidgetState();
}

class _AppTextFieldWidgetState extends State<AppTextFieldWidget> {
  final _focusNode = FocusNode();
  late TextEditingController _controller;
  final _keyForm = GlobalKey<FormState>();
  @override
  void initState() {
    _focusNode.addListener(focusListen);
    _controller = TextEditingController(text: widget.initValue);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant AppTextFieldWidget oldWidget) {
    if (oldWidget.initValue != widget.initValue) {
      _controller
        ..text = widget.initValue ?? ''
        ..selection = TextSelection.fromPosition(
            TextPosition(offset: (widget.initValue ?? '').length));
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _focusNode.removeListener(focusListen);
    _controller.dispose();
    super.dispose();
  }

  void focusListen() {
    if (!_focusNode.hasFocus) {
      if (_keyForm.currentState?.validate() ?? false) {
        _keyForm.currentState!.save();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _keyForm,
      child: TextFormField(
          onTap: widget.onTapField,
          enabled: widget.enabled,
          readOnly: widget.readOnly,
          textInputAction: widget.textInputAction,
          maxLength: widget.lengthText,
          onChanged: widget.onChanged,
          keyboardType: widget.keyboardType,
          onSaved: (value) => widget.onFieldSubmitted(value ?? ''),
          validator: widget.validator,
          controller: _controller,
          onFieldSubmitted: widget.onFieldSubmitted,
          focusNode: _focusNode,
          obscureText: widget.obsecureText ?? false,
          decoration: InputDecoration(
            hintText: widget.hintText,
            hintStyle: const TextStyle(
              color: AppColors.hintStyle,
            ),
            fillColor: widget.fillColor ?? AppColors.background2,
            filled: true,
            suffixIcon: (widget.showIconSvg ?? true)
                ? GestureDetector(
                    onTap: widget.onClickIcon,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: SvgPicture.asset(
                          widget.nameSvg ?? AppIcons.hidenPassword),
                    ),
                  )
                : const SizedBox(),
            suffixIconConstraints: const BoxConstraints(minWidth: 25),
            disabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            focusedErrorBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.vividRed),
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            errorBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.vividRed),
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
          )),
    );
  }
}
