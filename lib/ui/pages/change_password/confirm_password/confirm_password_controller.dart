import 'dart:convert';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/model/params/confirm_password_params.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/services/api/api_service.dart';
import 'package:g_office/services/auth_service.dart';
import 'package:g_office/ui/pages/login_user/login_page/logic/login_state.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class ConfirmChangePasswordController extends GetxController {
  ApiService apiService = Get.find();
  AuthService authService = Get.find();
  ButtonState buttonState = ButtonState(isActive: true);
  final isPassword = ShowPasswordState(obsecure: true);
  String password = '';
  String staffId = '';
  bool loaded = false;

  void onShowPassword() {
    isPassword.obsecure = !isPassword.obsecure;
    update();
  }

  void getValueInformationInPassword(String pass) {
    password = pass;
    buttonState.isActive = _validateButton();
    update();
  }

  bool _validateButton() {
    if (password.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future<void> confirmPassword() async {
    final String data =
        await AppLocalConfig.instance.getValue(LocalKeys.userInformation) ?? '';
    final SignInResponseEntity signInResponseEntity =
        SignInResponseEntity.fromJson(jsonDecode(data) as Map<String, dynamic>);
    final ConfirmPasswordParam _confirmPasswordParam = ConfirmPasswordParam(
      id: signInResponseEntity.staffId,
      password: password,
    );
    final mailCheck = await apiService.confirmPassword(_confirmPasswordParam);
    print(mailCheck.data);
    if (mailCheck.code == 200) {
      Get.offAndToNamed(RouteConfig.changePassword);
    } else {
      SnackbarErrorWidget.callSnackBar('Mật khẩu cũ không đúng');
    }
  }

  @override
  Future<void> onInit() async {
    loaded = false;
    buttonState = ButtonState(isActive: _validateButton());
    loaded = true;
    update();
    super.onInit();
  }
}
