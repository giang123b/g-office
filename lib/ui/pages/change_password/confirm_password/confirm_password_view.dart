import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/base_layout.dart';
import 'package:g_office/ui/pages/change_password/confirm_password/confirm_password_controller.dart';
import 'package:g_office/ui/widgets/elevated_button_widget/elevated_button_widget.dart';
import 'package:g_office/ui/widgets/text_field_widget/app_text_field_widget.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';

class ConfirmPasswordScreen extends StatelessWidget {
  ConfirmPasswordScreen({Key? key}) : super(key: key);
  final _inputController = Get.put(ConfirmChangePasswordController());
  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      tittle: 'Đặt lại mật khẩu',
      body: Column(
        children: [
          const SizedBox(
            height: 150,
          ),
          GetBuilder<ConfirmChangePasswordController>(
              init: _inputController,
              builder: (controller) {
                return AppTextFieldWidget(
                  initValue: controller.loaded ? controller.password : '',
                  onFieldSubmitted: (pass) {
                    controller.getValueInformationInPassword(pass);
                  },
                  onChanged: controller.getValueInformationInPassword,
                  hintText: 'Nhập mật khẩu hiện tại',
                  fillColor: AppColors.textWhite,
                  obsecureText: _inputController.isPassword.obsecure,
                  nameSvg: _inputController.isPassword.obsecure
                      ? AppIcons.hidenPassword
                      : AppIcons.showPassword,
                  onClickIcon: _inputController.onShowPassword,
                );
              }),
          const SizedBox(
            height: 40,
          ),
          GetBuilder<ConfirmChangePasswordController>(
            init: _inputController,
            builder: (controller) {
              return ElevatedButtonWidget(
                  informationButton: 'Tiếp tục',
                  onTap: () async {
                    await _inputController.confirmPassword();
                  },
                  state: _inputController.buttonState);
            },
          ),
        ],
      ),
    );
  }
}
