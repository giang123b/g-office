import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/base_layout.dart';
import 'package:g_office/ui/widgets/elevated_button_widget/elevated_button_widget.dart';
import 'package:g_office/ui/widgets/text_field_widget/app_text_field_widget.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import 'change_password_controller.dart';

class ChangePasswordPage extends StatelessWidget {
  ChangePasswordPage({Key? key}) : super(key: key);
  final _inputController = Get.put(ChangePasswordController());

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      tittle: 'Đặt lại mật khẩu',
      body: WillPopScope(
        onWillPop: () async => false,
        child: Column(
          children: [
            const SizedBox(height: 100),
            const Text(
              'Thay đổi mật khẩu mới',
              style: TextStyle(fontSize: 15.0),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 55),
            GetBuilder<ChangePasswordController>(
                init: _inputController,
                builder: (controller) {
                  return AppTextFieldWidget(
                    initValue:
                        controller.loaded ? controller.firstPassword : '',
                    onFieldSubmitted: (pass) {
                      controller.getValueInformationInFirstPassword(pass);
                    },
                    onChanged: controller.getValueInformationInFirstPassword,
                    hintText: 'Nhập mật khẩu mới',
                    fillColor: AppColors.textWhite,
                    obsecureText: _inputController.isFirstPassword.obsecure,
                    nameSvg: _inputController.isFirstPassword.obsecure
                        ? AppIcons.hidenPassword
                        : AppIcons.showPassword,
                    onClickIcon: _inputController.onShowFirstPassword,
                  );
                }),
            const SizedBox(
              height: 32,
            ),
            GetBuilder<ChangePasswordController>(
                init: _inputController,
                builder: (controller) {
                  return AppTextFieldWidget(
                    initValue:
                        controller.loaded ? controller.secondPassword : '',
                    onFieldSubmitted: (pass) {
                      controller.getValueInformationInSecondPassword(pass);
                    },
                    onChanged: controller.getValueInformationInSecondPassword,
                    hintText: 'Xác minh mật khẩu mới',
                    fillColor: AppColors.textWhite,
                    obsecureText: _inputController.isSecondPassword.obsecure,
                    nameSvg: _inputController.isSecondPassword.obsecure
                        ? AppIcons.hidenPassword
                        : AppIcons.showPassword,
                    onClickIcon: _inputController.onShowSecondPassword,
                  );
                }),
            const SizedBox(
              height: 45,
            ),
            GetBuilder<ChangePasswordController>(
              init: _inputController,
              builder: (controller) {
                return ElevatedButtonWidget(
                    informationButton: 'Lưu mật khẩu',
                    onTap: () async {
                      await _inputController.newPassword();
                    },
                    state: _inputController.buttonState);
              },
            ),
          ],
        ),
      ),
    );
  }
}
