import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/base_layout.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/ui/pages/home/home_root/home_root_logic.dart';
import 'package:g_office/ui/pages/home/tab_home/tab_home_logic.dart';
import 'package:g_office/ui/pages/home/tab_profile/profile_screen/tab_profile_logic.dart';
import 'package:g_office/ui/pages/home/tab_time_work/tab_time_work_logic.dart';
import 'package:g_office/ui/pages/login_user/login_page/logic/login_state.dart';
import 'package:g_office/ui/widgets/elevated_button_widget/elevated_button_widget.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:flutter_svg/svg.dart';
import '../change_password_controller.dart';

class ChangePasswordPageSuccess extends StatelessWidget {
  ChangePasswordPageSuccess({Key? key}) : super(key: key);
  final _inputController = Get.put(ChangePasswordController());
  final ButtonState buttonState = ButtonState(isActive: true);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: BaseLayout(
        body: Column(
          children: [
            const SizedBox(height: 200),
            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(50)),
                color: AppColors.greenSuccess,
              ),
              height: 100,
              width: 100,
              child: SvgPicture.asset(AppIcons.success),
            ),
            const SizedBox(height: 30),
            const Text(
              'Đặt lại mật khẩu thành công',
              style: TextStyle(
                  fontSize: 19,
                  color: Colors.black,
                  decoration: TextDecoration.none),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 140),
            GetBuilder<ChangePasswordController>(
              init: _inputController,
              builder: (controller) {
                return ElevatedButtonWidget(
                    informationButton: 'Xong',
                    onTap: () async {
                      Get.delete<HomeRootLogic>();
                      Get.delete<HomeRootLogic>();
                      Get.delete<TabHomeLogic>();
                      Get.delete<TabTimeWorkLogic>();
                      Get.delete<TabProfileLogic>();

                      Get.offAndToNamed(RouteConfig.login);
                    },
                    state: buttonState);
              },
            ),
          ],
        ),
      ),
    );
  }
}
