import 'dart:convert';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/model/params/change_password_param.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/services/api/api_service.dart';
import 'package:g_office/services/auth_service.dart';
import 'package:g_office/ui/pages/login_user/login_page/logic/login_state.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:get/get.dart';

class ChangePasswordController extends GetxController {
  ApiService apiService = Get.find();
  AuthService authService = Get.find();
  ButtonState buttonState = ButtonState(isActive: true);
  final isFirstPassword = ShowPasswordState(obsecure: true);
  final isSecondPassword = ShowPasswordState(obsecure: true);
  String firstPassword = '';
  String secondPassword = '';
  bool loaded = false;

  void onShowFirstPassword() {
    isFirstPassword.obsecure = !isFirstPassword.obsecure;
    update();
  }

  void onShowSecondPassword() {
    isSecondPassword.obsecure = !isSecondPassword.obsecure;
    update();
  }

  void getValueInformationInFirstPassword(String pass) {
    firstPassword = pass;
    buttonState.isActive = _validateButton();
    update();
  }

  void getValueInformationInSecondPassword(String pass) {
    secondPassword = pass;
    buttonState.isActive = _validateButton();
    update();
  }

  bool _validateButton() {
    if (firstPassword.isNotEmpty && secondPassword.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future<void> newPassword() async {
    final String data =
        await AppLocalConfig.instance.getValue(LocalKeys.userInformation) ?? '';
    final SignInResponseEntity signInResponseEntity =
        SignInResponseEntity.fromJson(jsonDecode(data) as Map<String, dynamic>);
    final PasswordParam _passwordParam = PasswordParam(
      id: signInResponseEntity.id,
      newPassword: firstPassword,
      confirmPassword: secondPassword,
    );
    final mailCheck = await apiService.changePassword(_passwordParam);
    if (mailCheck.code == 200 && firstPassword == secondPassword) {
      Get.offAndToNamed(RouteConfig.changePasswordSuccess);
    } else {
      SnackbarErrorWidget.callSnackBar('Mật khẩu chưa trùng nhau!');
    }
    //  update();
  }

  @override
  Future<void> onInit() async {
    loaded = false;
    buttonState = ButtonState(isActive: _validateButton());
    loaded = true;
    update();
    super.onInit();
  }
}
