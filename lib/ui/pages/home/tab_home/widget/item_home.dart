import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../tab_home_logic.dart';

class NewWidgetStack extends StatelessWidget {
  const NewWidgetStack({
    Key? key,
    required this.controller,
    required this.text,
    required this.backGroundColor,
    required this.foreGroundColor,
    required this.svgPic,
    required this.iconModule,
    required this.page,
  }) : super(key: key);
  final String text;
  final Color backGroundColor;
  final Color foreGroundColor;
  final String iconModule;
  final TabHomeLogic controller;
  final Widget svgPic;
  final String page;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(page);
      },
      child: Container(
        height: 105,
        width: double.infinity,
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: backGroundColor,
              offset: const Offset(0, 10),
              blurRadius: 1,
            ),
          ],
          color: foreGroundColor,
          borderRadius: const BorderRadius.all(Radius.circular(16)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SvgPicture.asset(iconModule),
                const SizedBox(height: 8),
                Text(
                  text,
                  style: const TextStyle(
                      fontSize: 17,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              width: 70,
              height: 70,
              child: svgPic,
            ),
          ],
        ),
      ),
    );
  }
}
