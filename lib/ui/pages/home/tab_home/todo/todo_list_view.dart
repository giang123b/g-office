import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/app_images.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/pages/home/tab_home/todo/widget/item_todo.dart';

class TodoListPage extends StatelessWidget {
  const TodoListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final paddingBottom = MediaQuery.of(context).viewPadding.bottom;
    return Scaffold(
      backgroundColor: AppColors.background2,
      appBar: AppBar(
        leading: Icon(Icons.arrow_back_ios),
        centerTitle: true,
        title: const Text('Thông tin cá nhân',
            style: AppCustomTextStyle.fontTitle),
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: SvgPicture.asset(AppIcons.icPlus),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16)
              .copyWith(bottom: paddingBottom + 34),
          child: Column(
            children: [
              const SizedBox(
                height: 38,
              ),
              ItemTodoList(
                content: "G Office",
                icon: AppImages.icLogoApp,
                callback: () {},
              ),
              const SizedBox(
                height: 16,
              ),
              ItemTodoList(
                content: "Gem tech",
                icon: AppImages.icLogoApp,
                callback: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}


