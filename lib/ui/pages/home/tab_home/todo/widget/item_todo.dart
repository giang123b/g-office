import 'package:flutter/material.dart';
import 'package:g_office/common/extentions/typedef.dart';


class ItemTodoList extends StatelessWidget {
  final String icon;
  final String content;
  final Callback callback;
  const ItemTodoList({
    required this.icon,
    required this.content,
    required this.callback,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        height: 60,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                SizedBox(
                  height: 24,
                  width: 24,
                  child: Image.asset(icon),
                ),
                const SizedBox(width: 8),
                Text(content),
              ],
            ),
            const Icon(Icons.arrow_forward_ios),
          ],
        ),
      ),
    );
  }
}
