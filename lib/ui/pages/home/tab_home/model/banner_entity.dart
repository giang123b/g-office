import 'package:json_annotation/json_annotation.dart';

part 'banner_entity.g.dart';

@JsonSerializable()
class BannerEntity {
  @JsonKey()
  String? id;
  @JsonKey()
  String? link;
  @JsonKey()
  String? image;

  BannerEntity({
    this.id,
    this.link,
    this.image,
  });

  factory BannerEntity.fromJson(Map<String, dynamic> json) =>
      _$BannerEntityFromJson(json);

  Map<String, dynamic> toJson() => _$BannerEntityToJson(this);
}
