import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/app_images.dart';
import 'package:g_office/common/base_layout.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/ui/pages/home/tab_home/tab_home_logic.dart';
import 'package:get/get.dart';

import 'widget/item_home.dart';

class TabHomePage extends StatelessWidget {
  final TabHomeLogic controller = Get.put(TabHomeLogic());

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      padding: 0,
      body: Stack(
        children: [
          Container(
            height: 95,
            padding:
                const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 30),
            decoration: const BoxDecoration(
                color: Colors.orange,
                image: DecorationImage(
                    image: AssetImage(AppImages.backGroundHome),
                    fit: BoxFit.cover)),
            child: Obx(
              () => Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(64),
                    child: SizedBox(
                      height: 40,
                      width: 40,
                      child: Image.network(
                        controller.state.imageAvatar.value != ''
                            ? controller.state.imageAvatar.value
                            : 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                  Text(
                    controller.state.fullname.value,
                    style: const TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 80),
            padding: const EdgeInsets.only(left: 20, right: 20),
            width: double.infinity,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0)),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: CarouselSlider(
                        items: [
                          Image.asset(
                            AppImages.imageBanner1,
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                          Image.asset(
                            AppImages.imageBanner2,
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                          Image.asset(
                            AppImages.imageBanner3,
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                          Image.asset(
                            AppImages.imageBanner4,
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                          Image.asset(
                            AppImages.imageBanner5,
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                        ],
                        options: CarouselOptions(
                            height: 160,
                            viewportFraction: 1,
                            autoPlay: true,
                            autoPlayInterval: const Duration(seconds: 2))),
                  ),
                  const SizedBox(height: 20),
                  // main choice module

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: NewWidgetStack(
                          page: RouteConfig.todoList,
                          controller: controller,
                          text: 'HRM',
                          backGroundColor: const Color(0xffC7E1F0),
                          foreGroundColor: const Color(0xff9DD1F0),
                          svgPic: SvgPicture.asset(
                            AppIcons.picHrm,
                          ),
                          iconModule: AppIcons.icHRM,
                        ),
                      ),
                      const SizedBox(width: 20),
                      Expanded(
                        child: NewWidgetStack(
                          page: RouteConfig.todoList,
                          controller: controller,
                          text: 'CRM',
                          backGroundColor: const Color(0xffFFCED3),
                          foreGroundColor: const Color(0xffF09999),
                          svgPic: SvgPicture.asset(
                            AppIcons.picCrm,
                          ),
                          iconModule: AppIcons.icCRM,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: NewWidgetStack(
                          page: RouteConfig.todoList,
                          controller: controller,
                          text: 'Todo',
                          backGroundColor: const Color(0xffFDD59D),
                          foreGroundColor: const Color(0xffFDEACE),
                          svgPic: SvgPicture.asset(
                            AppIcons.picTodo,
                          ),
                          iconModule: AppIcons.icTodo,
                        ),
                      ),
                      const SizedBox(width: 20),
                      Expanded(
                        child: NewWidgetStack(
                          page: RouteConfig.todoList,
                          controller: controller,
                          text: 'ACM',
                          backGroundColor: const Color(0xffB5D5B8),
                          foreGroundColor: const Color(0xffDBE7DC),
                          svgPic: SvgPicture.asset(
                            AppIcons.picAcm,
                          ),
                          iconModule: AppIcons.icACM,
                        ),
                      ),
                    ],
                  ),

                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: NewWidgetStack(
                          page: RouteConfig.todoList,
                          controller: controller,
                          text: 'Insight',
                          backGroundColor: const Color(0xffFFD686),
                          foreGroundColor: const Color(0xffFFEAB8),
                          svgPic: SvgPicture.asset(
                            AppIcons.picInsight,
                          ),
                          iconModule: AppIcons.icInsight,
                        ),
                      ),
                      const SizedBox(width: 20),
                      Expanded(
                        child: NewWidgetStack(
                          page: RouteConfig.todoList,
                          controller: controller,
                          text: 'Ticket',
                          backGroundColor: const Color(0xffAB9AD5),
                          foreGroundColor: const Color(0xffCBC1E3),
                          svgPic: SvgPicture.asset(
                            AppIcons.picTicket,
                          ),
                          iconModule: AppIcons.icTicket,
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  ),

                  const SizedBox(height: 20),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
