import 'dart:async';
import 'dart:convert';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/services/api/api_service.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:g_office/utils/logger.dart';
import 'package:get/get.dart';

import 'tab_home_state.dart';

class TabHomeLogic extends GetxController {
  final state = HomeTabState();
  ApiService apiService = Get.find();
  final ctiveindex = 1.obs;

  Future<void> getBanner() async {
    try {
      await apiService.getBanner();
    } catch (error) {
      logger.e(error);
    }
  }

  Future<void> setUpNameAndAvatar() async {
    final String data =
        await AppLocalConfig.instance.getValue(LocalKeys.userInformation) ?? '';
    final SignInResponseEntity signInResponseEntity =
        SignInResponseEntity.fromJson(jsonDecode(data) as Map<String, dynamic>);

    state.fullname.value = signInResponseEntity.staffName ?? '';
    state.imageAvatar.value = signInResponseEntity.avatar ?? '';
  }

  @override
  void onInit() {
    super.onInit();
    setUpNameAndAvatar();
  }
}
