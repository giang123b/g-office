import 'package:json_annotation/json_annotation.dart';

part 'check_in_request_entity.g.dart';

@JsonSerializable()
class CheckInRequestEntity {
  @JsonKey()
  String? staffId;
  @JsonKey()
  String? ipLogin;
  @JsonKey()
  String? ipBranch;

  CheckInRequestEntity(
      {required this.staffId, required this.ipLogin, required this.ipBranch});

  factory CheckInRequestEntity.fromJson(Map<String, dynamic> json) =>
      _$CheckInRequestEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CheckInRequestEntityToJson(this);
}
