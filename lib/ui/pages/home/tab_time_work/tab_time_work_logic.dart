import 'dart:async';
import 'dart:convert';

import 'package:dart_ipify/dart_ipify.dart';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:g_office/common/snack_bar/snackbar_success_widget.dart';
import 'package:g_office/configs/app_configs.dart';
import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/services/index.dart';
import 'package:g_office/ui/pages/home/tab_time_work/model/check_in_request_entity.dart';
import 'package:g_office/ui/pages/login_user/login_page/logic/login_state.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:g_office/utils/logger.dart';
import 'package:get/get.dart';
import 'tab_time_work_state.dart';

class TabTimeWorkLogic extends GetxController {
  final state = TabTimeWorkState();
  ApiService apiService = Get.find();
  AuthService authService = Get.find();
  final buttonState = ButtonState(isActive: true);
  late String _ip;
  late bool isCheckIn;
  @override
  Future<void> onInit() async {
    final time =
        await AppLocalConfig.instance.getValue(LocalKeys.dateTimeCheckIn);
    isCheckIn = time != AppConfigs.miliseconds.toString();
    _ip = await Ipify.ipv4();
    super.onInit();
  }

  Future<void> checkIn() async {
    try {
      final _saveId =
          await AppLocalConfig.instance.getValue(LocalKeys.keyCompany);
      final String data =
          await AppLocalConfig.instance.getValue(LocalKeys.userInformation) ??
              '';
      final SignInResponseEntity signInResponseEntity =
          SignInResponseEntity.fromJson(
              jsonDecode(data) as Map<String, dynamic>);
      final CheckInRequestEntity checkInRequestEntity = CheckInRequestEntity(
        staffId: signInResponseEntity.staffId,
        ipBranch: signInResponseEntity.ipBranch,
        ipLogin: _ip,
      );
      final result =
          await apiService.checkIn(_saveId ?? '', checkInRequestEntity);
      if (result.code == 200) {
        _setUpCheckIn();
        SnackbarSuccessWidget.callSnackBar('Bạn đã check in thành công');
      } else if (result.code == 201) {
        _setUpCheckIn();
        SnackbarErrorWidget.callSnackBar('Bạn đã check in trước đó rồi');
      } else {
        buttonState.isActive = false;
        _delayActive(1);
        SnackbarErrorWidget.callSnackBar('Check in thất bại');
      }
    } catch (error) {
      buttonState.isActive = false;
      _delayActive(1);
      SnackbarErrorWidget.callSnackBar('Check in thất bại');
    }
    update();
  }

  Future<void> _setUpCheckIn() async {
    isCheckIn = false;
    final String militime = AppConfigs.miliseconds.toString();
    await AppLocalConfig.instance
        .setValueAtLocal(LocalKeys.dateTimeCheckIn, militime);
  }

  Future<void> checkOut() async {
    try {
      final _saveId =
          await AppLocalConfig.instance.getValue(LocalKeys.keyCompany);
      final String data =
          await AppLocalConfig.instance.getValue(LocalKeys.userInformation) ??
              '';
      final SignInResponseEntity signInResponseEntity =
          SignInResponseEntity.fromJson(
              jsonDecode(data) as Map<String, dynamic>);
      final CheckInRequestEntity checkInRequestEntity = CheckInRequestEntity(
        staffId: signInResponseEntity.staffId,
        ipBranch: signInResponseEntity.ipBranch,
        ipLogin: _ip,
      );
      final result =
          await apiService.checkOut(_saveId ?? '', checkInRequestEntity);
      if (result.code == 200) {
        SnackbarSuccessWidget.callSnackBar(
            'Bạn đã check out thành công! 2 phút sau hãy quay lại');
        _delayActive(2);
      } else {
        SnackbarErrorWidget.callSnackBar(result.message);
      }
    } catch (error) {
      logger.e(error);
    }
    update();
  }

  void _delayActive(int time) {
    Future.delayed(Duration(minutes: time), () {
      buttonState.isActive = true;
      update();
    });
  }
}
