import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/pages/home/tab_time_work/tab_time_work_logic.dart';
import 'package:g_office/ui/widgets/elevated_button_widget/elevated_button_widget.dart';
import 'package:g_office/ui/widgets/logo/logo_widget.dart';
import 'package:get/get.dart';

class TabTimeWorkPage extends StatelessWidget {
  final _controller = Get.put(TabTimeWorkLogic());
  @override
  Widget build(BuildContext context) {
    final topbar = MediaQuery.of(context).padding.top;
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16)
            .copyWith(top: topbar + 60),
          child: Column(
            children: [
              const LogoWidget(),
              const SizedBox(height:50),
              const Text('Hướng dẫn',style: AppCustomTextStyle.fontHeadline2,),
              const SizedBox(height:400),
              const SizedBox(height: 48),
              GetBuilder<TabTimeWorkLogic>(
                init: _controller,
                  builder: (controller){
                return ElevatedButtonWidget(
                  color: _controller.isCheckIn?AppColors.frenchBlue:AppColors.main1,
                  informationButton: _controller.isCheckIn?'Check in':'Check out',
                  onTap: _controller.isCheckIn? _controller.checkIn:_controller.checkOut,
                  state: _controller.buttonState,
                );
              }),
              const SizedBox(height: 48),
            ],
          ),
        ),
      ),
    );
  }
}
