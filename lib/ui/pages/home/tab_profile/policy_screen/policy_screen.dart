import 'package:flutter/material.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/common/base_layout.dart';

class PolicyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      tittle: 'Điều khoản',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const [
          SizedBox(height: 20),
          Text(
            '1. Điều khoản bảo mật thông tin cá nhân',
            style: AppCustomTextStyle.BODY_MEDIUM,
          ),
          Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus.',
            style: AppCustomTextStyle.BODY_REGULAR,
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            '2. Điều khoản sử dụng dịch vụ',
            style: AppCustomTextStyle.BODY_MEDIUM,
          ),
          Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus.',
            style: AppCustomTextStyle.BODY_REGULAR,
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            '3. Điều khoản ứng dụng',
            style: AppCustomTextStyle.BODY_MEDIUM,
          ),
          Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus.',
            style: AppCustomTextStyle.BODY_REGULAR,
          ),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }
}
