import 'package:flutter/material.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/base_layout.dart';
import 'package:g_office/ui/pages/home/tab_profile/profile_screen/widgets/item_profile.dart';
import 'package:get/get.dart';

import 'contact_support_controller.dart';

class ContactSupportScreen extends StatelessWidget {
  final controller = Get.put(ContactSupportController());

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      tittle: 'Liên lạc',
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Container(
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Column(
              children: [
                ItemProfile(
                  content: "Website: https://gemstech.com.vn/",
                  icon: AppIcons.icContact,
                  callback: () =>
                      controller.goToWebsite('https://gemstech.com.vn/'),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 56),
                  child: Divider(
                    color: Colors.grey,
                    height: 0.5,
                    thickness: 0.1,
                  ),
                ),
                ItemProfile(
                  content: "Phone: 034 678 8118",
                  icon: AppIcons.icContact,
                  callback: () => controller.makeACall(),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
