import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactSupportController extends GetxController {
  Future<void> makeACall() async {
    const String _phoneNumber = 'tel:0346788118';
    if (await canLaunch(_phoneNumber)) {
      await launch(_phoneNumber);
    } else {
      SnackbarErrorWidget.callSnackBar('Vui lòng thử lại!');
    }
  }

  Future<void> goToWebsite(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      SnackbarErrorWidget.callSnackBar('Vui lòng thử lại!');
    }
  }
}
