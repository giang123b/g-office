// Flutter imports:
import 'package:flutter/material.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/base_layout.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/widget/avatar_image.dart';
import 'package:g_office/ui/pages/home/tab_profile/profile_screen/tab_profile_logic.dart';
import 'package:g_office/ui/pages/home/tab_profile/profile_screen/widgets/item_profile.dart';
import 'package:get/get.dart';

class TabProfilePage extends StatelessWidget {
  final TabProfileLogic controller = Get.put(TabProfileLogic());

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
        color: const Color(0xFFF5F6F7),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 64),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(
                    () => ClipRRect(
                      borderRadius: BorderRadius.circular(80),
                      child: SizedBox(
                        height: 80,
                        width: 80,
                        child: AvatarImage(
                          networkImage: controller.state.imageAvatar.value,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 24),
                  Text(
                    controller.state.fullname.value,
                    style: const TextStyle(color: Colors.black),
                  )
                ],
              ),
              const SizedBox(height: 24),
              Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  children: [
                    ItemProfile(
                      content: "Thông tin cá nhân",
                      icon: AppIcons.icUserInformation,
                      callback: () {
                        Get.toNamed(RouteConfig.profile);
                      },
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 56),
                      child: Divider(
                        color: Colors.grey,
                        height: 0.5,
                        thickness: 0.1,
                      ),
                    ),
                    ItemProfile(
                      content: "Thay đổi mật khẩu",
                      icon: AppIcons.icChangePassword,
                      callback: () {
                        Get.toNamed(RouteConfig.confirmChangePassword);
                      },
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 56),
                      child: Divider(
                        color: Colors.grey,
                        height: 0.5,
                        thickness: 0.1,
                      ),
                    ),
                    ItemProfile(
                      content: "Cài đặt sinh trắc học",
                      icon: AppIcons.icBiometric,
                      callback: () {},
                    )
                  ],
                ),
              ),
              const SizedBox(height: 32),
              Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  children: [
                    ItemProfile(
                      content: "Liên lạc",
                      icon: AppIcons.icContact,
                      callback: () => controller.goToContactScreen(),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 56),
                      child: Divider(
                        color: Colors.grey,
                        height: 0.5,
                        thickness: 0.1,
                      ),
                    ),
                    ItemProfile(
                      content: "Điều khoản",
                      icon: AppIcons.icPolicy,
                      callback: () => controller.goToPolicyScreen(),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 32),
              Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: ItemProfile(
                  content: "Đăng xuất",
                  icon: AppIcons.icLogOut,
                  callback: () => controller.logOut(),
                ),
              ),
              const SizedBox(height: 32),
            ],
          ),
        ));
  }
}
