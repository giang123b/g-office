import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/extentions/typedef.dart';

class ItemProfile extends StatelessWidget {
  final String icon;
  final String content;
  final Callback callback;
  const ItemProfile({
    required this.icon,
    required this.content,
    required this.callback,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        color: AppColors.textWhite,
        padding: const EdgeInsets.all(20),
        child: Row(
          children: [
            SvgPicture.asset(icon,fit: BoxFit.fitHeight,),
            const SizedBox(width: 8),
            Expanded(child: Text(content)),
            SvgPicture.asset(AppIcons.icRightArrow),
          ],
        ),
      ),
    );
  }
}
