import 'dart:convert';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/services/index.dart';
import 'package:g_office/ui/commons/app_dialog.dart';
import 'package:g_office/ui/pages/home/tab_profile/profile_screen/tab_profile_state.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:get/get.dart';

class TabProfileLogic extends GetxController {
  ApiService apiService = Get.find();
  AuthService authService = Get.find();
  final TabProfileState state = TabProfileState();

  Future<void> setUpNameAndAvatar() async {
    final String data =
        await AppLocalConfig.instance.getValue(LocalKeys.userInformation) ?? '';
    final SignInResponseEntity signInResponseEntity =
        SignInResponseEntity.fromJson(jsonDecode(data) as Map<String, dynamic>);

    state.fullname.value = signInResponseEntity.staffName ?? '';
    state.imageAvatar.value = signInResponseEntity.avatar ?? '';
  }

  Future<void> goToPolicyScreen() async {
    await Get.toNamed(RouteConfig.policy);
  }

  Future<void> goToContactScreen() async {
    await Get.toNamed(RouteConfig.contact);
  }

  Future<void> logOut() async {
    AppDialog.defaultDialog(
      title: 'Đăng xuất',
      message: 'Bạn có muốn đăng xuất không?',
      onConfirm: () {
        authService.removeToken();
        AppLocalConfig.instance.removeKey(LocalKeys.dateTimeCheckIn);
        AppLocalConfig.instance.removeKey(LocalKeys.isUseOnBoarding);
        AppLocalConfig.instance.removeKey(LocalKeys.userInformation);
        Get.offAllNamed(RouteConfig.login);
      },
    );
  }

  @override
  void onInit() {
    super.onInit();
    setUpNameAndAvatar();
  }
}
