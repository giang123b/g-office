import 'dart:convert';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:g_office/common/snack_bar/snackbar_success_widget.dart';
import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/database/local/city_data.dart';
import 'package:g_office/database/local/data_local.dart';
import 'package:g_office/model/params/city_param.dart';
import 'package:g_office/model/params/profile_param.dart';
import 'package:g_office/model/params/update_param.dart';
import 'package:g_office/services/api/api_profile_service.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:g_office/utils/utils.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:g_office/common/app_date_format.dart';

import 'profie_state.dart';

class ProfileLogic extends GetxController {
  /// State profile
  ProfileState profileState = ProfileState();
  final ApiProfileService _profileService = Get.find();
  ProfileParam profile = ProfileParam();
  UpdateParam _updateParam = UpdateParam();
  late SignInResponseEntity _signInResponseEntity;

  /// State Edit
  bool isEdit = true;
  List<CityParam> cityData = [];
  String _idCompany = '';
  @override
  Future<void> onInit() async {
    cityData = await CityDataLocal.instance.init() as List<CityParam>;
    final dataUser = await AppLocalConfig.instance.getValue(
      LocalKeys.userInformation,
    );
    _idCompany =
        await AppLocalConfig.instance.getValue(LocalKeys.keyCompany) ?? '';

    ///Cal User Local
    if (dataUser != null) {
      final dataUserLocalParse = jsonDecode(dataUser);

      ///Decode data user local
      _signInResponseEntity = SignInResponseEntity.fromJson(
          dataUserLocalParse as Map<String, dynamic>);

      ///Get api user
      final dataProfileUser = await _profileService.getProfileUser(
          _idCompany,
          _signInResponseEntity.token ?? '',
          {"staffId": _signInResponseEntity.staffId});
      if (dataProfileUser.data != null &&
          dataProfileUser.data!.isNotEmpty &&
          dataProfileUser.code == 200) {
        profile = dataProfileUser.data![0];
        profileState = ProfileState(
            name: profile.name,
            address: profile.address,
            email: profile.email,
            city: profile.province.getNameCity(cityData),
            idAddress: profile.idAddress.getNameCity(cityData),
            gender: genders[int.parse(profile.gender ?? '0')],
            birthDay: profile.birthDay.parseStringToDateTimeYMD,
            idDate: profile.idDate.parseStringToDateTimeYMD,
            idCard: profile.idCard,
            vssId: profile.vssId,
            national: profile.nationality,
            pathAvatar: profile.avatar,
            initIdAddress: _getInitValue(profile.idAddress),
            initIdProvince: _getInitValue(profile.province));
        _getInformationUserParam();
      }
    } else {
      SnackbarErrorWidget.callSnackBar('Dữ liệu bị lỗi');
    }

    update();
    super.onInit();
  }

  Future<void> editProfile() async {
    isEdit = !isEdit;
    final bool isSave = ValidatorUtils.isEmail(_updateParam.email ?? '') &&
        ValidatorUtils.isPhoneNumber(_updateParam.phoneNumber ?? '') &&
        _updateParam.name != null && _updateParam.name!.isNotEmpty;
    if (isEdit) {
      if (isSave) {
        final data = await _profileService.updateProfile(_idCompany,
            _signInResponseEntity.token ?? '', _updateParam.toJson());

        if (data.code == 200) {
          SnackbarSuccessWidget.callSnackBar(data.message);
        } else {
          SnackbarErrorWidget.callSnackBar(data.message);
        }
      } else {
        isEdit = false;
        SnackbarErrorWidget.callSnackBar('Hãy nhập đủ trường dữ liệu');
      }
    }
    update();
  }

  void getGender(String selectedGender) {
    profileState.gender = selectedGender;
    for (final gender in genders) {
      if (selectedGender == gender) {
        _updateParam.gender = genders.indexOf(gender).toString();
      }
    }
    update();
  }
  void getNational(String national){
    _updateParam.nationality = national.trim();
  }
  void getValueName(String name) {
    _updateParam.name = name.trim();
  }

  void getPhoneNumber(String phoneNumber) {
    _updateParam.phoneNumber = phoneNumber.trim();
  }

  void getEmail(String email) {
    _updateParam.email = email.trim();
  }

  void getAddress(String address) {
    _updateParam.address = address.trim();
  }

  void getBirthday(DateTime dateTime) {
    profileState.birthDay = dateTime;
    _updateParam.birthDay = dateTime.parseDateTimeToStringYMD;
    update();
  }

  void getIdDate(DateTime dateTime) {
    profileState.idDate = dateTime;
    _updateParam.idDate = dateTime.parseDateTimeToStringYMD;
    update();
  }

  void getProvince(int id) {
    for (final city in cityData) {
      if (id == cityData.indexOf(city)) {
        profileState.city = city.name;
        _updateParam.province = city.id;
      }
    }
    update();
  }

  int _getInitValue(String? id) {
    for (final city in cityData) {
      if (id == city.id) {
        return cityData.indexOf(city);
      }
    }
    return 0;
  }

  void getIdAddress(int id) {
    for (final city in cityData) {
      if (id == cityData.indexOf(city)) {
        profileState.idAddress = city.name;
        profileState.initIdAddress = cityData.indexOf(city);
        _updateParam.idAddress = city.id;
      }
    }
    update();
  }

  void getIdCard(String idCard) {
    _updateParam.idCard = idCard.trim();
  }
  void clearEmail(){profileState.email = '';_updateParam.email = '';
  update();}
  void clearName(){profileState.name = '';_updateParam.name = '';update();}
  void getVssId(String vssId){
    _updateParam.vssId = vssId.trim();
  }
  void _getInformationUserParam() {
    _updateParam = UpdateParam(
      staffId: _signInResponseEntity.staffId,
      name: profile.name,
      email: profile.email,
      gender: profile.gender,
      birthDay: profile.birthDay,
      address: profile.address,
      phoneNumber: profile.phoneNumber,
      province: profile.province,
      residence: profile.residence,
      idCard: profile.idCard,
      idDate: profile.idDate,
      idAddress: profile.idAddress,
      taxCode: profile.taxCode,
      maritalStatus: profile.maritalStatus,
      nationality: profile.nationality,
      description: profile.description,
      vssId: profile.vssId,
      avatar: profile.avatar,
    );
  }
}
