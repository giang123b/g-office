class ProfileState {
  String? gender;
  DateTime? birthDay;
  String? city;
  String? idAddress;
  DateTime? idDate;
  String? idCard;
  String? address;
  String? name;
  int? initIdProvince;
  int? initIdAddress;
  String? vssId;
  String? national;
  String? pathAvatar;
  String? email;
  ProfileState(
      {this.gender,
      this.city,
      this.idAddress,
      this.birthDay,
      this.name,
      this.idDate,
      this.address,
      this.initIdAddress,
      this.initIdProvince,
        this.vssId,
        this.national,
      this.idCard,this.pathAvatar,this.email});
}
