import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

import 'profile_logic/profile_logic.dart';
import 'widget/body_profile_screen.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key? key}) : super(key: key);
  final _profileController = Get.put(ProfileLogic());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background2,
      appBar: AppBar(
        //leading: SvgPicture.asset(AppIcons.arrowLeft),
        centerTitle: true,
        title: const Text('Thông tin cá nhân',
            style: AppCustomTextStyle.fontTitle),
        elevation: 0,
        actions: [
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: _profileController.editProfile,
            child: SizedBox(
              width: 60,
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Align(
                    child: GetBuilder<ProfileLogic>(
                  init: _profileController,
                  builder: (controller) {
                    return Text(controller.isEdit ? 'Sửa' : 'Lưu',
                        style: AppCustomTextStyle.fontTitle);
                  },
                )),
              ),
            ),
          )
        ],
      ),
      body: BodyProfileScreen(),
    );
  }
}
