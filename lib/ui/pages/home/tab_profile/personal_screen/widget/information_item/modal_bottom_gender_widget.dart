import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';

class ModalBottonGenderWidget extends StatelessWidget {
  const ModalBottonGenderWidget({
    Key? key,
    required this.onBackData,
  }) : super(key: key);
  final Function(String) onBackData;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () {
          Get.back();
        },
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: const EdgeInsets.only(bottom: 40, left: 12, right: 12),
            height: 111,
            width: 390,
            decoration: BoxDecoration(
              color: const Color(0xffF5F6F7),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    onBackData('Nam');
                    Get.back();
                  },
                  child: SizedBox(
                    width: 390,
                    height: 55,
                    child: Center(
                      child: Text('Nam',
                          style: AppCustomTextStyle.fontBigStyle
                              .copyWith(fontSize: 17)),
                    ),
                  ),
                ),
                Container(
                  height: 1,
                  color: AppColors.textWhite,
                ),
                InkWell(
                  onTap: () {
                    onBackData('Nữ');
                    Get.back();
                  },
                  child: SizedBox(
                    width: 390,
                    height: 55,
                    child: Center(
                      child: Text('Nữ',
                          style: AppCustomTextStyle.fontBigStyle
                              .copyWith(fontSize: 17)),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
