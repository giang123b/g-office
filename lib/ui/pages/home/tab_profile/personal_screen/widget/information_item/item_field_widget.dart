import 'package:flutter/material.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/widgets/text_field_widget/app_text_field_widget.dart';

class ItemFieldWidget extends StatelessWidget {
  const ItemFieldWidget(
      {Key? key,
      required this.title,
      this.paddingBottom,
      this.readOnly,
      this.onTap,
      this.enabled,
      this.initValue,
      this.validator,
      this.onSubmitted,
      this.nameSvg,
      this.onChanged,
      this.keyboardType, this.onClickIcon, this.showIcon})
      : super(key: key);
  final String title;
  final String? nameSvg;
  final bool? enabled;
  final double? paddingBottom;
  final bool? readOnly;
  final Function()? onTap;
  final String? initValue;
  final String? Function(String?)? validator;
  final Function(String)? onSubmitted;
  final Function(String)? onChanged;
  final TextInputType? keyboardType;
  final Function()? onClickIcon;
  final bool? showIcon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: paddingBottom ?? 16.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          title,
          style: AppCustomTextStyle.fontHeadline1,
        ),
        const SizedBox(
          height: 8,
        ),
        AppTextFieldWidget(
          onClickIcon: onClickIcon,
          nameSvg: nameSvg,
          keyboardType: keyboardType,
          enabled: enabled,
          onTapField: onTap,
          readOnly: readOnly ?? false,
          onFieldSubmitted: onSubmitted ?? (value) {},
          showIconSvg: showIcon??false,
          initValue: initValue,
          validator: validator,
          onChanged: onChanged,
        )
      ]),
    );
  }
}
