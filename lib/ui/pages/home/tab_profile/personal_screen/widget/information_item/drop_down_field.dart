import 'package:flutter/material.dart';

import 'item_field_widget.dart';

class ItemShowBottomSheet extends StatelessWidget {
  const ItemShowBottomSheet({
    required this.onChangedBottomSheet,
    this.title,
    this.enabled, this.showIcon, this.nameSvg,
  });
  final Function() onChangedBottomSheet;
  final String? title;
  final bool? enabled;
  final bool?showIcon;
  final String? nameSvg;
  @override
  Widget build(BuildContext context) {
    return ItemFieldWidget(
      showIcon: showIcon,
      nameSvg: nameSvg,
      enabled: enabled,
      paddingBottom: 8,
      title: 'Giới tính',
      readOnly: true,
      onTap: onChangedBottomSheet,
      initValue: title,
    );
  }
}
