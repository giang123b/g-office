import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/instance_manager.dart';

class BottomModalPicker extends StatelessWidget {
  const BottomModalPicker(
      {Key? key, this.onChangedDateTime, this.initDateTime, this.child})
      : super(key: key);
  final Function(DateTime)? onChangedDateTime;
  final DateTime? initDateTime;
  final Widget? child;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
              decoration: const BoxDecoration(
                  color: AppColors.textWhite,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12),
                  )),
              alignment: Alignment.centerRight,
              padding: const EdgeInsets.only(right: 16),
              width: double.infinity,
              height: 40,
              child: Text(
                'Xong',
                style: AppCustomTextStyle.fontTitle
                    .copyWith(color: AppColors.frenchBlue),
              )),
        ),
        child ??
            SizedBox(
              height: 260,
              child: CupertinoDatePicker(
                  initialDateTime: initDateTime,
                  backgroundColor: AppColors.textWhite,
                  onDateTimeChanged: onChangedDateTime ?? (dateTime) {},
                  maximumDate: DateTime.now(),
                  minimumYear: 1900,
                  mode: CupertinoDatePickerMode.date,
                  maximumYear: DateTime.now().year),
            ),
      ],
    );
  }
}
