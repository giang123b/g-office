import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/ui/widgets/circle_avatar_icon/circle_avatar_widget.dart';

class AvatarImage extends StatelessWidget {
  const AvatarImage({Key? key, this.openImagePicker, this.networkImage = ''})
      : super(key: key);
  final Function()? openImagePicker;
  final String networkImage;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: openImagePicker,
      child: CircleAvatar(
        maxRadius: 40,
        child: Image.network(
            networkImage,
            errorBuilder: (_, __, ___) {
              return const CircleAvatarWidget(
                colorBackground: AppColors.silverSand,
                linkIcon: AppIcons.personal,
              );
            }),
      ),
    );
  }
}
