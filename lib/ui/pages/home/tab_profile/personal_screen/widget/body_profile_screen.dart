import 'package:flutter/material.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/profile_logic/profile_logic.dart';
import 'package:get/get.dart';
import 'avatar_image.dart';
import 'field_information/field_add_information_widget.dart';
import 'field_information/field_communications_widget.dart';
import 'field_information/field_information_widget.dart';
import 'field_information/field_legacy_widget.dart';

class BodyProfileScreen extends StatelessWidget {
  BodyProfileScreen({Key? key}) : super(key: key);
  final _profileController = Get.put(ProfileLogic());
  @override
  Widget build(BuildContext context) {
    final paddingBottom = MediaQuery.of(context).viewPadding.bottom;
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16)
            .copyWith(bottom: paddingBottom + 34),
        child: Column(
          children: [
            Padding(
                padding: const EdgeInsets.only(top: 21, bottom: 32),
                child: GetBuilder<ProfileLogic>(
                  init: _profileController,
                  builder: (controller) {
                    return AvatarImage(
                      networkImage: controller.profileState.pathAvatar??'',
                    );
                  },
                )),
            FieldInformationWidget(),
            const SizedBox(
              height: 32,
            ),
            FieldCommunicationsWidget(),
            const SizedBox(
              height: 32,
            ),
            FieldLegacyWidget(),
            const SizedBox(
              height: 32,
            ),
            const FieldAddInformationWidget()
          ],
        ),
      ),
    );
  }
}
