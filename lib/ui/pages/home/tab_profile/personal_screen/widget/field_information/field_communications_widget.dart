import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/profile_logic/profile_logic.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/widget/information_item/item_field_widget.dart';
import 'package:g_office/utils/utils.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

class FieldCommunicationsWidget extends StatelessWidget {
  FieldCommunicationsWidget({Key? key}) : super(key: key);
  final _profileController = Get.put(ProfileLogic());
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: AppColors.textWhite,
        borderRadius: BorderRadius.all(Radius.circular(12.0) //
            ),
      ),
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(bottom: 16),
            child: Text(
              'Thông tin liên lạc',
              style: AppCustomTextStyle.fontTitle,
            ),
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                validator: (value) {
                  if (value != null && !ValidatorUtils.isEmail(value)) {
                    return 'Hãy nhập đúng địa chỉ email';
                  } else if (value == null) {
                    return 'Hãy nhập địa chỉ email';
                  }
                  return null;
                },
                showIcon: !controller.isEdit,
                nameSvg: AppIcons.icRemove,
                onClickIcon: controller.clearEmail,
                enabled: !controller.isEdit,
                initValue: controller.profileState.email,
                onChanged: controller.getEmail,
                title: 'Email',
              );
            },
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                validator: (value) {
                  if (value != null && !ValidatorUtils.isPhoneNumber(value)) {
                    return 'Hãy nhập đúng ký tự số điện thoại';
                  } else if (value == null) {
                    return 'Hãy nhập số điện thoại';
                  } else if (value.length < 9 || value.length > 11) {
                    return 'Hãy nhập đũ kí tự số điện thoại';
                  }
                  return null;
                },
                keyboardType: TextInputType.number,
                enabled: !controller.isEdit,
                initValue: controller.profile.phoneNumber,
                title: 'Số điện thoại',
                onChanged: controller.getPhoneNumber,
              );
            },
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                  enabled: !controller.isEdit,
                  initValue: controller.profileState.address,
                  title: 'Địa chỉ thường trú',
                  onChanged: controller.getAddress);
            },
          ),
        ],
      ),
    );
  }
}
