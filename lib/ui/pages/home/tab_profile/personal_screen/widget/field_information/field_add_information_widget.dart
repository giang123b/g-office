import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/widget/information_item/item_field_widget.dart';

class FieldAddInformationWidget extends StatelessWidget {
  const FieldAddInformationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: AppColors.textWhite,
        borderRadius: BorderRadius.all(Radius.circular(12.0) //
            ),
      ),
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(bottom: 16),
            child: Text(
              'Thông tin thêm',
              style: AppCustomTextStyle.fontTitle,
            ),
          ),
          ItemFieldWidget(
            enabled: false,
            title: 'Ghi chú',
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
