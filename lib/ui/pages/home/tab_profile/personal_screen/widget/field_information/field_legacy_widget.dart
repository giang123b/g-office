import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/profile_logic/profile_logic.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/widget/information_item/item_field_widget.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/widget/information_item/modal_date_picker.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:g_office/common/app_date_format.dart';

class FieldLegacyWidget extends StatelessWidget {
  FieldLegacyWidget({Key? key}) : super(key: key);
  final _profileController = Get.put(ProfileLogic());
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: AppColors.textWhite,
        borderRadius: BorderRadius.all(Radius.circular(12.0) //
            ),
      ),
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(bottom: 16),
            child: Text(
              'Thông tin pháp lý',
              style: AppCustomTextStyle.fontTitle,
            ),
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                keyboardType: TextInputType.number,
                enabled: !controller.isEdit,
                initValue: controller.profileState.idCard,
                onChanged: controller.getIdCard,
                title: 'Số CCCD',
              );
            },
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                showIcon: !controller.isEdit,
                nameSvg: AppIcons.icCalendar,
                enabled: !controller.isEdit,
                readOnly: true,
                initValue:
                    controller.profileState.idDate.parseDateTimeToStringDMY ??
                        '',
                title: 'Ngày cấp',
                onTap: () {
                  Get.bottomSheet(BottomModalPicker(
                    initDateTime: controller.profileState.idDate
                        .parseDateTimeToStringDMY.parseStringToDateTimeDMY,
                    onChangedDateTime: controller.getIdDate,
                  ));
                },
              );
            },
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                enabled: !controller.isEdit,
                readOnly: true,
                initValue: controller.profileState.idAddress,
                title: 'Nơi cấp',
                onTap: () {
                  Get.bottomSheet(
                    BottomModalPicker(
                      child: SizedBox(
                        height: 260,
                        child: CupertinoPicker(
                          scrollController: FixedExtentScrollController(
                              initialItem:
                                  controller.profileState.initIdAddress ?? 0),
                          backgroundColor: AppColors.textWhite,
                          onSelectedItemChanged: controller.getIdAddress,
                          itemExtent: 64,
                          children: controller.cityData
                              .map((e) => Center(child: Text((e.name) ?? '')))
                              .toList(),
                        ),
                      ),
                    ),
                  );
                },
              );
            },
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            initState: (_) {},
            builder: (controller) {
              return ItemFieldWidget(
                enabled: !controller.isEdit,
                title: 'Mã số thuế',
                initValue: controller.profileState.vssId,
                onChanged: controller.getVssId,
              );
            },
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                enabled: !controller.isEdit,
                initValue: controller.profileState.national,
                title: 'Quốc tịch',
                onChanged: controller.getNational,
              );
            },
          ),
        ],
      ),
    );
  }
}
