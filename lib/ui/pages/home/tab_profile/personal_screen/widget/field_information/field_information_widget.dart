import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/profile_logic/profile_logic.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/widget/information_item/item_field_widget.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/widget/information_item/modal_bottom_gender_widget.dart';
import 'package:g_office/ui/pages/home/tab_profile/personal_screen/widget/information_item/modal_date_picker.dart';
import 'package:get/get.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:g_office/common/app_date_format.dart';
import '../information_item/drop_down_field.dart';

class FieldInformationWidget extends StatelessWidget {
  FieldInformationWidget({Key? key}) : super(key: key);
  final _profileController = Get.put(ProfileLogic());
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: AppColors.textWhite,
        borderRadius: BorderRadius.all(Radius.circular(12.0) //
            ),
      ),
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(bottom: 16),
            child: Text(
              'Thông tin chung',
              style: AppCustomTextStyle.fontTitle,
            ),
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                validator: (value) {
                  if (value == null || value.trim().isEmpty) {
                    return "Nhập đầy đủ họ và tên";
                  }
                  return null;
                },
                onChanged: _profileController.getValueName,
                enabled: !controller.isEdit,
                title: 'Họ và tên',
                initValue: controller.profileState.name,
                showIcon: !controller.isEdit,
                nameSvg: AppIcons.icRemove,
                onClickIcon: controller.clearName,
              );
            },
          ),
          GetBuilder<ProfileLogic>(
              init: _profileController,
              builder: (controller) {
                return ItemShowBottomSheet(
                  showIcon: !controller.isEdit,
                  nameSvg: AppIcons.icDropDown,
                  enabled: !controller.isEdit,
                  title: controller.profileState.gender,
                  onChangedBottomSheet: () {
                    showGeneralDialog(
                      barrierColor: AppColors.textBlack.withOpacity(0.5),
                      transitionDuration: const Duration(milliseconds: 700),
                      context: context,
                      pageBuilder: (context, anim1, anim2) {
                        return ModalBottonGenderWidget(
                            onBackData: controller.getGender);
                      },
                      transitionBuilder: (context, anim1, anim2, child) {
                        return SlideTransition(
                          position:
                              Tween(begin: const Offset(0, 1), end: Offset.zero)
                                  .animate(anim1),
                          child: child,
                        );
                      },
                    );
                  },
                );
              }),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                showIcon: !controller.isEdit,
                nameSvg: AppIcons.icCalendar,
                enabled: !controller.isEdit,
                readOnly: true,
                title: 'Ngày sinh',
                initValue:
                    controller.profileState.birthDay.parseDateTimeToStringDMY ??
                        '',
                onTap: () {
                  Get.bottomSheet(BottomModalPicker(
                    initDateTime: controller.profileState.birthDay
                        .parseDateTimeToStringDMY.parseStringToDateTimeDMY,
                    onChangedDateTime: controller.getBirthday,
                  ));
                },
              );
            },
          ),
          GetBuilder<ProfileLogic>(
            init: _profileController,
            builder: (controller) {
              return ItemFieldWidget(
                readOnly: true,
                initValue: controller.profileState.city,
                enabled: !controller.isEdit,
                paddingBottom: 0,
                title: 'Quê quán',
                onTap: () {
                  Get.bottomSheet(
                    BottomModalPicker(
                      child: SizedBox(
                        height: 260,
                        child: CupertinoPicker(
                          scrollController: FixedExtentScrollController(
                              initialItem:
                                  controller.profileState.initIdProvince ?? 0),
                          backgroundColor: AppColors.textWhite,
                          onSelectedItemChanged: controller.getProvince,
                          itemExtent: 64,
                          children: controller.cityData
                              .map((e) => Center(child: Text((e.name) ?? '')))
                              .toList(),
                        ),
                      ),
                    ),
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
