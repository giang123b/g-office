import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/ui/pages/home/home_root/home_root_logic.dart';
import 'package:g_office/ui/pages/home/tab_home/tab_home_view.dart';
import 'package:g_office/ui/pages/home/tab_notification/tab_notification_view.dart';
import 'package:g_office/ui/pages/home/tab_profile/profile_screen/tab_profile_view.dart';
import 'package:g_office/ui/pages/home/tab_time_work/tab_time_work_view.dart';

import 'package:get/get.dart';

class HomeRootPage extends StatelessWidget {
  final HomeRootLogic controller = Get.put(HomeRootLogic());
  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: <Widget>[
          TabHomePage(),
          TabTimeWorkPage(),
          TabNotificationPage(),
          TabProfilePage(),
        ],
        onPageChanged: (pageIndex) {
          controller.state.currentPage.value = pageIndex;
        },
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: controller.state.currentPage.value != 0 ? SvgPicture.asset(AppIcons.icHome): SvgPicture.asset(AppIcons.icHomeSelected),
              label: 'Trang chủ',
            ),
            BottomNavigationBarItem(
              icon: controller.state.currentPage.value != 1 ? SvgPicture.asset(AppIcons.icWorkTime): SvgPicture.asset(AppIcons.icWorkTimeSelected),
              label: 'Chấm công',
            ),
            BottomNavigationBarItem(
              icon: controller.state.currentPage.value != 2 ? SvgPicture.asset(AppIcons.icNotification): SvgPicture.asset(AppIcons.icNotificationSelected),
              label: 'Thông báo',
            ),
            BottomNavigationBarItem(
              icon: controller.state.currentPage.value != 3 ? SvgPicture.asset(AppIcons.icProfile): SvgPicture.asset(AppIcons.icProfileSelected),
              label: 'Cá nhân',
            ),
          ],
          currentIndex: controller.state.currentPage.value,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.grey,
          onTap: (int pageIndex) {
            controller.state.currentPage.value = pageIndex;
            _pageController.jumpToPage(pageIndex);
          },
        ),
      ),
    );
  }
}
