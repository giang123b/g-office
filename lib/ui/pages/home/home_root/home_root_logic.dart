import 'package:g_office/services/api/api_profile_service.dart';
import 'package:g_office/ui/pages/home/home_root/home_root_state.dart';
import 'package:get/get.dart';

class HomeRootLogic extends GetxController {
  ApiProfileService profileService = Get.find();
  HomeRootState state = HomeRootState();

  @override
  void onInit() {
    fetch();
    super.onInit();
  }

  dynamic fetch() async {}
}
