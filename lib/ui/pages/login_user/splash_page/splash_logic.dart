import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/services/auth_service.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  String isUseOnBoarding = "false";
  String isHasCodeCompany = "false";
  String isHasToken = "false";
  final AuthService _authService = Get.find();

  @override
  Future<void> onInit() async {
    await _checkCompanyNumber();
    super.onInit();
  }

  Future<void> _checkCompanyNumber() async {
    final keyCompany =
        await AppLocalConfig.instance.getValue(LocalKeys.keyCompany);
    final token = await _authService.getToken();
    if (keyCompany == null || keyCompany.isEmpty) {
      Get.offAndToNamed(RouteConfig.inputCompany);
    } else if (token == null || token.isEmpty) {
      Get.offAndToNamed(RouteConfig.login);
    } else {
      Get.offAllNamed(RouteConfig.homeRoot);
    }
  }
}
