import 'package:flutter/material.dart';
import 'package:g_office/ui/pages/login_user/splash_page/splash_logic.dart';
import 'package:g_office/ui/widgets/logo/logo_widget.dart';
import 'package:get/get.dart';

class SplashScreen extends StatelessWidget {
  SplashScreen({Key? key}) : super(key: key);
  final splashController = Get.put(SplashController());
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        body: Center(
      child: LogoWidget(
        widthLogo: 200,
        heightLogo: 90,
      ),
    ));
  }
}
