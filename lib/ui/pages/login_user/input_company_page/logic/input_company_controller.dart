import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/services/api/api_company_service.dart';
import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:g_office/ui/pages/login_user/login_page/logic/login_state.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class InputCompanyController extends GetxController {
  ApiCompanyService companyService = Get.find();
  ButtonState buttonState = ButtonState(isActive: true);
  String value = '';
  bool loaded = false;
  @override
  Future<void> onInit() async {
    loaded = false;
    value =
        await AppLocalConfig.instance.getValue(LocalKeys.keySavedIdCompany) ??
            '';
    buttonState = ButtonState(isActive: _validateButton());
    loaded = true;
    update();
    super.onInit();
  }

  void getValueCompany(String inputCode) {
    value = inputCode;
    buttonState = ButtonState(isActive: _validateButton());
    update();
  }

  bool _validateButton() {
    if (value.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future<void> navigateLoginScreen() async {
    final company = await companyService.getCompany(value);

    /// Check status when back end change status code
    if (company.code == 200) {
      await AppLocalConfig.instance
          .setValueAtLocal(LocalKeys.keyCompany, value);
      Get.toNamed(RouteConfig.login);
    } else {
      SnackbarErrorWidget.callSnackBar(company.message ?? '');
    }
  }
}
