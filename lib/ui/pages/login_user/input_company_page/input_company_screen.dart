import 'package:flutter/material.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/widgets/elevated_button_widget/elevated_button_widget.dart';
import 'package:g_office/ui/widgets/logo/logo_widget.dart';
import 'package:g_office/ui/widgets/text_field_widget/app_text_field_widget.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import 'logic/input_company_controller.dart';

class InputCompanyScreen extends StatelessWidget {
  final _inputController = Get.put(InputCompanyController());
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(children: [
              const Spacer(),
              const LogoWidget(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 32),
                child: Text(
                  'Nhập mã số doanh nghiệp của bạn',
                  style: AppCustomTextStyle.fontBigStyle.copyWith(fontSize: 17),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(bottom: 32),
                  child: GetBuilder<InputCompanyController>(
                    init: _inputController,
                    builder: (controller) {
                      return AppTextFieldWidget(
                        initValue: controller.loaded ? controller.value : '',
                        onFieldSubmitted: _inputController.getValueCompany,
                        showIconSvg: false,
                        onChanged: _inputController.getValueCompany,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Hãy nhập ký tự";
                          }
                          return null;
                        },
                      );
                    },
                  )),
              GetBuilder<InputCompanyController>(
                init: _inputController,
                builder: (_) {
                  return ElevatedButtonWidget(
                      informationButton: 'Tiếp tục',
                      onTap: () async {
                        await _inputController.navigateLoginScreen();
                      },
                      state: _inputController.buttonState);
                },
              ),
              const Spacer(),
            ]),
          ),
        ),
      ),
    );
  }
}
