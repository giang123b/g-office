import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/services/api/api_service.dart';
import 'package:g_office/services/index.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class OnBoardingLogic extends GetxController {
  ApiService apiService = Get.find();
  AuthService authService = Get.find();

  Future<void> saveIsUseOnBoarding() async {
    await AppLocalConfig.instance
        .setValueAtLocal(LocalKeys.isUseOnBoarding, 'true');
    Get.offAndToNamed(RouteConfig.inputCompany);
  }
}
