import 'package:flutter/material.dart';
import 'package:g_office/common/app_images.dart';
import 'package:g_office/common/base_layout.dart';
import 'package:g_office/ui/pages/login_user/onboarding/on_boarding_logic.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';

class OnBoardingPage extends StatelessWidget {
  final _controllerState = Get.put(OnBoardingLogic());

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);

    const pageDecoration = PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return BaseLayout(
      body: IntroductionScreen(
        pages: [
          PageViewModel(
            title: "ACM",
            body: "G-Office giúp bạn quản lý tài chính của công ty...",
            image: Image.asset(AppImages.imageBanner2),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: "HRM",
            body: "Bạn có thể quản lý nhân sự một cách dễ dàng",
            image: Image.asset(AppImages.imageBanner3),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: "CRM",
            body:
                "Bạn sẽ có những cách tuyệt vời để xây dựng mối quan hệ với khách hàng",
            image: Image.asset(AppImages.imageBanner1),
            decoration: pageDecoration,
          ),
        ],
        onDone: () => _controllerState.saveIsUseOnBoarding(),
        //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
        showSkipButton: true,
        skipFlex: 0,
        nextFlex: 0,
        color: Colors.black,

        //rtl: true, // Display as right-to-left
        skip: const Text('Bỏ qua'),
        next: const Icon(Icons.arrow_forward),
        done: const Text('Tiếp tục',
            style: TextStyle(fontWeight: FontWeight.w600)),
        curve: Curves.fastLinearToSlowEaseIn,
        controlsMargin: const EdgeInsets.all(16),
        controlsPadding: const EdgeInsets.all(12.0),
        dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          color: Color(0xFFBDBDBD),
          activeSize: Size(22.0, 10.0),
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
        ),
        dotsContainerDecorator: const ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
        ),
      ),
    );
  }
}
