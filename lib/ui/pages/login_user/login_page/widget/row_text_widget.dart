import 'package:flutter/material.dart';
import 'package:g_office/common/app_text_styles.dart';

class ButtonTextWidget extends StatelessWidget {
  const ButtonTextWidget({Key? key, this.onTap}) : super(key: key);
  final Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
            color: Colors.white,
            width: 150,
            child: Align(
              alignment: Alignment.centerRight,
              child: Text('Quên mật khẩu ?',
                  style:
                      AppCustomTextStyle.fontBigStyle.copyWith(fontSize: 15)),
            )),
      ),
    );
  }
}
