class ButtonState {
  bool isActive;

  ButtonState({required this.isActive});
}

class ShowPasswordState {
  bool obsecure;

  ShowPasswordState({required this.obsecure});
}
