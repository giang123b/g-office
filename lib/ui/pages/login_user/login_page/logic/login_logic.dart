import 'dart:convert';
import 'package:g_office/common/app_local_key.dart';
import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:g_office/configs/app_local_config.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/services/api/api_service.dart';
import 'package:g_office/services/index.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:g_office/utils/logger.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

import 'login_state.dart';

class LoginLogic extends GetxController {
  ApiService apiService = Get.find();
  AuthService authService = Get.find();
  final buttonState = ButtonState(isActive: false);
  final isPassword = ShowPasswordState(obsecure: true);
  String account = '';
  String password = '';

  void onShowPassword() {
    isPassword.obsecure = !isPassword.obsecure;
    update();
  }

  void getValueInformationPassword(String pass) {
    password = pass;
    buttonState.isActive = _validateButton();
    update();
  }

  Future<void> goToForgotPasswordScreen() async {
    await Get.toNamed(RouteConfig.forgotPassword);
  }

  void getValueInformationAccount(String acc) {
    account = acc;
    buttonState.isActive = _validateButton();
    update();
  }

  bool _validateButton() {
    if (password.isNotEmpty && account.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future<void> onSubmitted() async {
    final _saveId =
    await AppLocalConfig.instance.getValue(LocalKeys.keyCompany);
    try {
      final result = await apiService.login(_saveId??'',account, password);
      final SignInResponseEntity? signInResponseEntity = result.data;
      if (signInResponseEntity?.token != null &&
          signInResponseEntity!.token!.isNotEmpty) {
        await AppLocalConfig.instance.setValueAtLocal(
            LocalKeys.userInformation, jsonEncode(signInResponseEntity));
        authService.saveToken(signInResponseEntity.token ?? '');
        Get.offAllNamed(RouteConfig.homeRoot);
      } else {
        SnackbarErrorWidget.callSnackBar(result.message ?? '');
      }
    } catch (error) {
      SnackbarErrorWidget.callSnackBar(
          'Vui lòng kiểm tra thông tin đăng nhập!');
      logger.e(error);
    }
  }

  Future<void> onEditScreen() async {
    final _saveId =
        await AppLocalConfig.instance.getValue(LocalKeys.keyCompany);
    await AppLocalConfig.instance
        .setValueAtLocal(LocalKeys.keySavedIdCompany, _saveId ?? '');
    await AppLocalConfig.instance.removeKey(LocalKeys.keyCompany);
    Get.offNamed(RouteConfig.inputCompany);
  }
}
