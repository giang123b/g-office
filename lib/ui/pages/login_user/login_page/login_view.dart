import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/app_icons.dart';
import 'package:g_office/common/app_text_styles.dart';
import 'package:g_office/ui/widgets/elevated_button_widget/elevated_button_widget.dart';
import 'package:g_office/ui/widgets/logo/logo_widget.dart';
import 'package:g_office/ui/widgets/text_field_widget/app_text_field_widget.dart';
import 'package:get/instance_manager.dart';
import 'package:get/state_manager.dart';
import 'logic/login_logic.dart';
import 'widget/row_text_widget.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);
  final _controllerState = Get.put(LoginLogic());
  @override
  Widget build(BuildContext context) {
    final topbar = MediaQuery.of(context).padding.top;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16)
                .copyWith(top: topbar + 60),
            child: Column(children: [
              const LogoWidget(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 32.0),
                child: Column(
                  children: [
                    const Text('Chào mừng đến với G Office',
                        style: AppCustomTextStyle.fontBigStyle),
                    const SizedBox(
                      height: 16,
                    ),
                    Text('Đăng nhập để bắt đầu phiên làm việc',
                        style: AppCustomTextStyle.fontBigStyle
                            .copyWith(fontSize: 15)),
                  ],
                ),
              ),
              AppTextFieldWidget(
                textInputAction: TextInputAction.next,
                onChanged: _controllerState.getValueInformationAccount,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Tài khoản không được rỗng';
                  }
                },
                initValue: _controllerState.account,
                hintText: 'Email/ Tên đăng nhập',
                showIconSvg: false,
                onFieldSubmitted: (value) {
                  _controllerState.getValueInformationAccount(value);
                },
              ),
              const SizedBox(
                height: 32,
              ),
              GetBuilder<LoginLogic>(
                  init: _controllerState,
                  builder: (controller) {
                    return AppTextFieldWidget(
                      initValue: _controllerState.password,
                      onChanged: _controllerState.getValueInformationPassword,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Mật khẩu không được rỗng';
                        }
                      },
                      hintText: 'Mật khẩu',
                      onFieldSubmitted: (value) {
                        _controllerState.getValueInformationPassword(value);
                      },
                      obsecureText: _controllerState.isPassword.obsecure,
                      nameSvg: _controllerState.isPassword.obsecure
                          ? AppIcons.hidenPassword
                          : AppIcons.showPassword,
                      onClickIcon: _controllerState.onShowPassword,
                    );
                  }),
              Padding(
                  padding: const EdgeInsets.only(top: 24, bottom: 37),
                  child: ButtonTextWidget(
                    onTap: () async {
                      await _controllerState.goToForgotPasswordScreen();
                    },
                  )),
              Padding(
                  padding: const EdgeInsets.only(bottom: 32),
                  child: GetBuilder<LoginLogic>(
                    init: _controllerState,
                    builder: (controller) {
                      return ElevatedButtonWidget(
                        state: controller.buttonState,
                        informationButton: 'Đăng nhập',
                        onTap: () async {
                          await _controllerState.onSubmitted();
                        },
                      );
                    },
                  )),
              OutlinedButton(
                style: OutlinedButton.styleFrom(
                  side: const BorderSide(color: AppColors.frenchBlue),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  minimumSize: const Size(double.infinity, 64),
                ),
                onPressed: () async {
                  await _controllerState.onEditScreen();
                },
                child: Text(
                  'Thay đổi doanh nghiệp',
                  style: AppCustomTextStyle.fontBigStyle
                      .copyWith(color: AppColors.frenchBlue, fontSize: 15),
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
