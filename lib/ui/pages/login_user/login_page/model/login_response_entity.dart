import 'package:json_annotation/json_annotation.dart';

part 'login_response_entity.g.dart';

@JsonSerializable()
class SignInResponseEntity {
  @JsonKey()
  String? id;
  @JsonKey()
  String? email;
  @JsonKey()
  String? staffId;
  @JsonKey()
  String? department;
  @JsonKey()
  String? token;
  @JsonKey()
  String? extNum;
  @JsonKey()
  String? sipPass;
  @JsonKey()
  String? staffName;
  @JsonKey()
  String? avatar;
  @JsonKey()
  String? ipBranch;

  SignInResponseEntity(
      {this.id,
      this.email,
      this.staffId,
      this.department,
      this.token,
      this.extNum,
      this.sipPass,
      this.staffName,
      this.avatar,
      this.ipBranch});

  factory SignInResponseEntity.fromJson(Map<String, dynamic> json) =>
      _$SignInResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => _$SignInResponseEntityToJson(this);
}
