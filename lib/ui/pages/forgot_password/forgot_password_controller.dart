import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:g_office/model/params/email_param.dart';
import 'package:g_office/services/api/api_service.dart';
import 'package:g_office/ui/pages/login_user/login_page/logic/login_state.dart';
import 'package:get/get.dart';

class ForgotPasswordController extends GetxController {
  ApiService apiPasswordService = Get.find();
  final EmailParam _emailParam = EmailParam();
  ButtonState buttonState = ButtonState(isActive: true);
  String account = '';
  bool loaded = false;

  void getValueInformationAccount(String acc) {
    account = acc;
    _emailParam.email = account;
    buttonState.isActive = _validateButton();
    update();
  }

  bool _validateButton() {
    if (account.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future<void> getEmail() async {
    final mailCheck =
        await apiPasswordService.forgetUserPassword(_emailParam.toJson());
    if (mailCheck.code == 200) {
      print(_emailParam.email);
      //   Get.toNamed(RouteConfig.changePassword);
    } else {
      SnackbarErrorWidget.callSnackBar('Địa chỉ email không đúng');
    }
    //  update();
  }

  @override
  Future<void> onInit() async {
    loaded = false;
    buttonState = ButtonState(isActive: _validateButton());
    loaded = true;
    update();
    super.onInit();
  }
}
