import 'package:flutter/material.dart';
import 'package:g_office/common/app_colors.dart';
import 'package:g_office/common/base_layout.dart';
import 'package:g_office/ui/widgets/elevated_button_widget/elevated_button_widget.dart';
import 'package:g_office/ui/widgets/text_field_widget/app_text_field_widget.dart';
import 'package:get/get.dart';
import 'forgot_password_controller.dart';

class ForgetPasswordPage extends StatelessWidget {
  final _inputController = Get.put(ForgotPasswordController());
  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      tittle: 'Đặt lại mật khẩu',
      body: WillPopScope(
        onWillPop: () async => false,
        child: Column(
          children: [
            const SizedBox(height: 100),
            const Text(
              'Vui lòng nhập địa chỉ Email và điền mã xác minh được gửi kèm để xác nhận thay đổi mật khẩu',
              style: TextStyle(fontSize: 15.0),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 32),
            GetBuilder<ForgotPasswordController>(
                init: _inputController,
                builder: (controller) {
                  return AppTextFieldWidget(
                    initValue: controller.loaded ? controller.account : '',
                    onFieldSubmitted: (acc) {
                      controller.getValueInformationAccount(acc);
                    },
                    onChanged: controller.getValueInformationAccount,
                    showIconSvg: false,
                    hintText: 'Nhập địa chỉ email',
                    fillColor: AppColors.textWhite,
                  );
                }),
            const SizedBox(
              height: 40,
            ),
            GetBuilder<ForgotPasswordController>(
              init: _inputController,
              builder: (controller) {
                return ElevatedButtonWidget(
                    informationButton: 'Lấy mã',
                    onTap: () async {
                      await _inputController.getEmail();
                    },
                    state: _inputController.buttonState);
              },
            ),
          ],
        ),
      ),
    );
  }
}
