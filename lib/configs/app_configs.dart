class AppConfigs {
  static const String appName = 'Real Agent';

  ///STAGING
  static const envName = "Staging";
  static const baseUrl = "https://velo.vn/goffice/api/0109537138";

  ///Paging
  static const pageSize = 20;
  static const pageSizeMax = 1000;

  ///Local
  static const appLocal = 'vi_VN';
  static const appLanguage = 'en';

  ///DateFormat

  static const dateAPIFormat = 'dd/MM/yyyy';
  static const dateDisplayFormat = 'dd/MM/yyyy';

  static const dateTimeAPIFormat =
      "MM/dd/yyyy'T'hh:mm:ss.SSSZ"; //Use DateTime.parse(date) instead of ...
  static const dateTimeDisplayFormat = 'dd/MM/yyyy HH:mm';

  ///Date range
  static final identityMinDate = DateTime(1900);
  static final identityMaxDate = DateTime.now();
  static final birthMinDate = DateTime(1900);
  static final birthMaxDate = DateTime.now();
  static int miliseconds = DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day).microsecondsSinceEpoch;
  ///Font
  static const fontFamily = 'Roboto';

  ///Max file
  static const maxAttachFile = 5;

  ///Write check api
  static const apiCompanyUrl = "https://velo.vn/goffice/api/";
}

class DatabaseConfig {
  //Todo
  static const int version = 1;
}
