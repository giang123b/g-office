import 'package:shared_preferences/shared_preferences.dart';

class AppLocalConfig {
  AppLocalConfig._appLocal();

  static final AppLocalConfig instance = AppLocalConfig._appLocal();

  Future<void> setValueAtLocal(String key, String value) async {
    final pref = await SharedPreferences.getInstance();
    pref.setString(key, value);
  }

  Future<void> removeAllKey() async {
    final pref = await SharedPreferences.getInstance();
    pref.clear();
  }

  Future<void> removeKey(String key) async {
    final pref = await SharedPreferences.getInstance();
    pref.remove(key);
  }

  Future<String?> getValue(String key) async {
    final pref = await SharedPreferences.getInstance();
    return pref.getString(key);
  }
}
