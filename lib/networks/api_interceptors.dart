import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:g_office/common/snack_bar/snackbar_error_widget.dart';
import 'package:g_office/router/route_config.dart';
import 'package:g_office/services/index.dart';
import 'package:g_office/ui/widgets/loading/loading_screen.dart';
import 'package:g_office/utils/logger.dart';
import 'package:get/get.dart' hide Response;

class ApiInterceptors extends InterceptorsWrapper {
  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    LoadingScreenApp.instance.showDialogLoading();
    final authService = Get.find<AuthService>();

    final token = await authService.getToken() ?? '';

    options.queryParameters['token'] = token;
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    LoadingScreenApp.instance.hide();
    if (response.statusCode == 401) {
      final authService = Get.find<AuthService>();
      authService.signOut();
      Get.offAndToNamed(RouteConfig.login);
    }

    super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    final statusCode = err.response?.statusCode;
    final uri = err.requestOptions.path;
    var data = "";
    LoadingScreenApp.instance.hide();
    try {
      data = jsonEncode(err.response?.data);
      SnackbarErrorWidget.callSnackBar('Lỗi load dữ liệu');
    } catch (e) {
      SnackbarErrorWidget.callSnackBar(e.toString());
    }
    logger.log("⚠️ ERROR[$statusCode] => PATH: $uri\n DATA: $data");
    super.onError(err, handler);
  }
}
