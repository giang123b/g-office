import 'package:dio/dio.dart' hide Headers;
import 'package:g_office/configs/app_configs.dart';
import 'package:g_office/model/entities/index.dart';
import 'package:g_office/model/params/email_param.dart';
import 'package:g_office/model/response/array_base_response.dart';
import 'package:g_office/model/response/array_response.dart';
import 'package:g_office/model/response/base_reponse.dart';
import 'package:g_office/ui/pages/home/tab_home/model/banner_entity.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:retrofit/retrofit.dart';

part 'api_client.g.dart';

@RestApi(baseUrl: AppConfigs.apiCompanyUrl)
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @POST("/{id}/auth/login")
  Future<BaseResponseEntity<SignInResponseEntity>> login(
    @Path("id") String id,
    @Query('username') String username,
    @Query('password') String password,
  );

  @POST("/system/banner")
  Future<BaseResponseEntity<BannerEntity>> getBanner();

  /// Notification
  @POST("/notifications")
  Future<ArrayResponse<NotificationEntity>> getNotifications(
    @Query('page') int page,
    @Query('pageSize') int pageSize,
  );

  @POST("/{id}/chamcong/checkinwifi")
  Future<BaseResponseEntity<List<dynamic>>> checkIn(
      @Path("id") String id,
      @Query('staffId') String staffId,
      @Query('ipLogin') String ipLogin,
      @Query('ipBranch') String ipBranch);

  @POST("/{id}/chamcong/checkout")
  Future<BaseResponseEntity<List<dynamic>>> checkOut(
      @Path("id") String id,
      @Query('staffId') String staffId,
      @Query('ipLogin') String ipLogin,
      @Query('ipBranch') String ipBranch);

  //Set for demo: 'gemstech'
  @POST("/gemstech/system/forgetPassword")
  @Headers(<String, dynamic>{
    "Content-Type": "multipart/form-data",
  })
  Future<ArrayBaseResponseEntity<EmailParam>> forgetPassword(
      @Body() Map<String, dynamic> email);

  //Set for demo: 'gemstech'
  @POST("/gemstech/system/changePassword")
  @Headers(<String, dynamic>{
    "Content-Type": "multipart/form-data",
  })
  Future<BaseResponseEntity<List<dynamic>>> changePassword(
      @Query('staffId') String staffId,
      @Query('newPassword') String newPassword,
      @Query('confirmPassword') String confirmPassword);

  @POST("/system/checkOldPassword")
  @Headers(<String, dynamic>{
    "Content-Type": "multipart/form-data",
  })
  Future<BaseResponseEntity<List<dynamic>>> confirmPassword(
      @Query('staffId') String staffId, @Query('password') String password);
}
