import 'package:dio/dio.dart';
import 'package:g_office/configs/app_configs.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'api_client.dart';
import 'api_interceptors.dart';

class ApiUtil {
  static Dio? dio;

  static Dio getDio() {
    if (dio == null) {
      dio = Dio();
      dio!.options.connectTimeout = 60000;
      dio!.interceptors.add(ApiInterceptors());
      dio!.interceptors
          .add(PrettyDioLogger(requestHeader: true, requestBody: true));
    }
    return dio!;
  }

  static ApiClient getApiClient() {
    final apiClient = ApiClient(getDio(), baseUrl: AppConfigs.apiCompanyUrl);
    return apiClient;
  }
}
