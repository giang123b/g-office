import 'package:dio/dio.dart' hide Headers;
import 'package:g_office/configs/app_configs.dart';
import 'package:g_office/model/params/profile_param.dart';
import 'package:g_office/model/params/update_param.dart';
import 'package:g_office/model/response/array_base_response.dart';
import 'package:retrofit/http.dart';
part 'api_profile.g.dart';

@RestApi(baseUrl: AppConfigs.apiCompanyUrl)
abstract class ProfileService {
  factory ProfileService(Dio dio, {String baseUrl}) = _ProfileService;

  @POST('/{id}/nhansu/profile')
  @Headers(<String, dynamic>{
    "Content-Type": "multipart/form-data",
  })
  Future<ArrayBaseResponseEntity<ProfileParam>> getProfile(
    @Path("id") String id,
    @Query("token") String token,
    @Body() Map<String, dynamic> idStaff,
  );

  @POST('/{id}/nhansu/updateProfile')
  @Headers(<String, dynamic>{
    "Content-Type": "multipart/form-data",
  })
  Future<ArrayBaseResponseEntity<UpdateParam>> upDateProfile(
    @Path("id") String id,
    @Query("token") String token,
    @Body() Map<String, dynamic> profile,
  );
}
