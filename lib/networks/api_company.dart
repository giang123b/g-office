import 'package:dio/dio.dart';
import 'package:g_office/configs/app_configs.dart';
import 'package:g_office/model/params/company_param.dart';
import 'package:retrofit/retrofit.dart';
part 'api_company.g.dart';

@RestApi(baseUrl: AppConfigs.apiCompanyUrl)
abstract class ApiCompanyClient {
  factory ApiCompanyClient(Dio dio, {String baseUrl}) = _ApiCompanyClient;

  @POST('/{id}')
  Future<CompanyParam> getCompany(
    @Path("id") String id,
  );
}
