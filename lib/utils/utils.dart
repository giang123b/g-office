import 'package:get/get.dart';

class ValidatorUtils {
  /// Checks if string is email.
  static bool isEmail(String email) => GetUtils.isEmail(email);

  /// Checks if string is phone number.
  static bool isPhoneNumber(String phoneNumber) {
    if(phoneNumber.length > 12||phoneNumber.length < 9){
      return false;
    }
    return RegExp(r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$').hasMatch(phoneNumber);
  }

  /// Checks if string is URL.
  static bool isURL(String url) => GetUtils.isURL(url);
}
