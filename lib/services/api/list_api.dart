part of 'api_service.dart';

extension ListApiService on ApiService {
  Future<BaseResponseEntity<SignInResponseEntity>> login(
      String idCompany, String username, String password) async {
    return _apiClient.login(idCompany, username, password);
  }

  Future<BaseResponseEntity> checkIn(
      String idCompany, CheckInRequestEntity checkInRequestEntity) async {
    return _apiClient.checkIn(
        idCompany,
        checkInRequestEntity.staffId ?? '',
        checkInRequestEntity.ipLogin ?? '',
        checkInRequestEntity.ipBranch ?? '');
  }

  Future<BaseResponseEntity> checkOut(
      String idCompany, CheckInRequestEntity checkInRequestEntity) async {
    return _apiClient.checkOut(
        idCompany,
        checkInRequestEntity.staffId ?? '',
        checkInRequestEntity.ipLogin ?? '',
        checkInRequestEntity.ipBranch ?? '');
  }

  Future<BaseResponseEntity<BannerEntity>> getBanner() async {
    return _apiClient.getBanner();
  }

  Future<BaseResponseEntity> changePassword(PasswordParam passwordParam) async {
    return _apiClient.changePassword(passwordParam.id ?? '',
        passwordParam.newPassword ?? '', passwordParam.confirmPassword ?? '');
  }

  Future<BaseResponseEntity> confirmPassword(
      ConfirmPasswordParam confirmPasswordParam) async {
    return _apiClient.confirmPassword(
        confirmPasswordParam.id ?? '', confirmPasswordParam.password ?? '');
  }
}
