import 'package:g_office/configs/app_configs.dart';
import 'package:g_office/model/entities/index.dart';
import 'package:g_office/model/params/change_password_param.dart';
import 'package:g_office/model/params/confirm_password_params.dart';
import 'package:g_office/model/params/email_param.dart';
import 'package:g_office/model/response/array_base_response.dart';
import 'package:g_office/model/response/array_response.dart';
import 'package:g_office/model/response/base_reponse.dart';
import 'package:g_office/networks/api_client.dart';
import 'package:g_office/networks/api_util.dart';
import 'package:g_office/ui/pages/home/tab_home/model/banner_entity.dart';
import 'package:g_office/ui/pages/home/tab_time_work/model/check_in_request_entity.dart';
import 'package:g_office/ui/pages/login_user/login_page/model/login_response_entity.dart';
import 'package:get/get.dart';

part 'auth_api.dart';

part 'list_api.dart';

part 'notification_api.dart';

part 'api_password_service.dart';

class ApiService extends GetxService {
  late ApiClient _apiClient;

  Future<ApiService> init() async {
    _apiClient = ApiUtil.getApiClient();
    return this;
  }
}
