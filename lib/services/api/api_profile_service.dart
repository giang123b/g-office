import 'package:g_office/configs/app_configs.dart';
import 'package:g_office/model/params/profile_param.dart';
import 'package:g_office/model/params/update_param.dart';
import 'package:g_office/model/response/array_base_response.dart';
import 'package:g_office/networks/api_profile.dart';
import 'package:g_office/networks/api_util.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class ApiProfileService extends GetxService {
  late ProfileService _apiClient;

  Future<ApiProfileService> init() async {
    _apiClient =
        ProfileService(ApiUtil.getDio(), baseUrl: AppConfigs.apiCompanyUrl);
    return this;
  }
}

extension ExtensionApiCompanyService on ApiProfileService {
  Future<ArrayBaseResponseEntity<ProfileParam>> getProfileUser(
      String id, String token, Map<String, dynamic> idStaff) async {
    return _apiClient.getProfile(id, token, idStaff);
  }

  Future<ArrayBaseResponseEntity<UpdateParam>> updateProfile(
      String id, String token, Map<String, dynamic> profile) async {
    return _apiClient.upDateProfile(id, token, profile);
  }
}
