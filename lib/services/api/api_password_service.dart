
part of 'api_service.dart';


extension ExtensionApiPasswordService on ApiService{
  Future<ArrayBaseResponseEntity<EmailParam>> forgetUserPassword(Map<String, dynamic> email) async{
    return _apiClient.forgetPassword(email);
  }

}