import 'package:g_office/model/params/company_param.dart';
import 'package:g_office/networks/api_company.dart';
import 'package:g_office/networks/api_util.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class ApiCompanyService extends GetxService {
  late ApiCompanyClient _apiClient;

  Future<ApiCompanyService> init() async {
    _apiClient = ApiCompanyClient(ApiUtil.getDio());
    return this;
  }
}

extension ExtensionApiCompanyService on ApiCompanyService {
  Future<CompanyParam> getCompany(String id) async {
    return _apiClient.getCompany(id);
  }
}
