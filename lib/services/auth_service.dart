import 'package:g_office/database/local/secure_storage_helper.dart';
import 'package:get/get.dart';

class AuthService extends GetxService {
  Future<AuthService> init() async {
    return this;
  }

  /// Handle save/remove Token
  dynamic saveToken(String token) {
    return SecureStorageHelper.getInstance().saveToken(token);
  }

  /// Handle save/remove Token
  Future<String?> getToken() {
    return SecureStorageHelper.getInstance().getToken();
  }

  Future<void> removeToken() {
    return SecureStorageHelper.getInstance().removeToken();
  }

  /// SignOut
  Future<void> signOut() async {
    removeToken();
    // deleteUser();
  }
}
